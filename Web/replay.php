<?php
	require_once("action/ReplayAction.php");
	$action = new ReplayAction();
	$action->execute();

	$_SESSION["css_extra"] = array("css/replay.css");

	require_once("partial/header.php");
?>
	
	<script src="js/util/glMatrix-0.9.5.min.js"></script>
	<script src="js/webgl/shader.js"></script>
	<script src="js/webgl/objet3d.js"></script>
	<script src="js/webgl/arbre3d.js"></script>
	<script src="js/webgl/balle3d.js"></script>
	<script src="js/webgl/ciel3d.js"></script>
	<script src="js/webgl/item3d.js"></script>
	<script src="js/webgl/mur3d.js"></script>
	<script src="js/webgl/tank3d.js"></script>
	<script src="js/webgl/map3d.js"></script>

	<script src="js/interface/infojoueur.js"></script>

	<script src="js/entity/map.js"></script>
	<script src="js/entity/tuile.js"></script>
	<script src="js/replay.js"></script>

	<div id="replay">
		<canvas id="map3d" width="1000" height="700"> </canvas>
		<canvas id="infos_joueurs" width="1000" height="700"> </canvas>
	</div>
	<div id="lecture_replay">
		<input id="slider" type="range" min="0"/>
		<button id="bouton_lecture">Pause</button>
	</div>
	
<?php
	require_once("partial/footer.php");