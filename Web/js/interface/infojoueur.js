class InfoJoueur {
	constructor(canvas) {
		this.canvas = canvas
		this.contexte = this.initialiser_contexte(canvas)
		this.info_joueur = null
		this.id_gagnant = null

		let largeur_canvas = canvas.width
		let marge = 20

		this.y_texte = 40
		this.y_barre_vie = 50
		this.largeur_barre_vie_max = 350
		this.liste_x = {1:marge, 2:(largeur_canvas-marge-this.largeur_barre_vie_max)}
		
		this.hauteur_barre_vie = 40
	}

	initialiser_contexte(canvas) {
		return canvas.getContext("2d")
	}

	initialiser_info(info_joueur, id_gagnant) {
		this.info_joueur = info_joueur
		this.id_gagnant = id_gagnant
	}

	afficher(liste_enr_tank, fin_partie) {
		this.contexte.clearRect(0, 0, this.canvas.width, this.canvas.height)
		// On affiche les infos de chaque joueur
		liste_enr_tank.forEach((tank) => {
			let id = tank.id
			let vie = tank.vie
			let vie_max = this.info_joueur[id].vie_max
			let no = this.info_joueur[id].no
			let nom = this.info_joueur[id].nom
			let couleur = this.info_joueur[id].couleur
			
			// affichage de la barre de vie
			this.contexte.fillStyle = "black"
			this.contexte.fillRect(this.liste_x[no], this.y_barre_vie, this.largeur_barre_vie_max, this.hauteur_barre_vie)

			// affichage de la vie
			if(!fin_partie || id == this.id_gagnant) { // on veut que la vie du perdant soit à 0
				let largeur_barre_vie = (this.largeur_barre_vie_max-2)*vie/vie_max
				this.contexte.fillStyle = "rgba("+parseInt(couleur[0]*255)+","+parseInt(couleur[1]*255)+","+parseInt(couleur[2]*255)+", 1.0)"
				let marge = 8
				this.contexte.fillRect(this.liste_x[no]+marge, this.y_barre_vie+marge, largeur_barre_vie-marge*2, this.hauteur_barre_vie-marge*2)
			}
			// affichage du nom du joueur
			this.contexte.fillStyle = "black"
			this.contexte.font = "bold 32px Arial"
			this.contexte.fillText(nom, this.liste_x[no], this.y_texte)

			// Si c'est la fin de la partie, alors on affiche le gagnant
			if(fin_partie && id == this.id_gagnant) {
				let texte = "Joueur "+nom+" a gagné, !"
				let hauteur_texte = 60
				
				this.contexte.font = "bold "+hauteur_texte.toString()+"px Arial"
				this.contexte.shadowColor="black";
				this.contexte.shadowBlur = 7;
				this.contexte.lineWidth = 5;
				let largeur_texte = this.contexte.measureText(texte).width
				let pos_x = this.canvas.width/2-largeur_texte/2
				let pos_y = this.canvas.height/2-hauteur_texte/2
				this.contexte.strokeText(texte,pos_x, pos_y);
				this.contexte.shadowBlur = 0;
				this.contexte.fillStyle = "rgba(2, 51, 178, 1.0)"
				this.contexte.fillText(texte, pos_x, pos_y)
			}
		})

		
	}
}