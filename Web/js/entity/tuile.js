class Tuile {
	constructor(x, y) {
		this.x = x
		this.y = y
		this.type = Tuile.correspondance_type["plancher"]
		this.arbre = false
	}

	modifier(type, arbre) {
		this.type = type
		this.arbre = arbre
	}
}

// Constantes
Tuile.choix_type = ["plancher", "mur", "mobile", "mobile inversé"]
Tuile.correspondance_type = {
	"plancher": "1",
	"mur": "2",
	"mobile": "3",
	"mobile inversé": "4"
}