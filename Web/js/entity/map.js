class Map {
	constructor(largeur, hauteur, liste_tuile_temp) {
		this.nom = null
		this.largeur = Number(largeur)
		this.hauteur = Number(hauteur)
		this.liste_tuile = this.initialiser_tuiles(liste_tuile_temp)
	}

	initialiser_tuiles(liste_tuile_temp) {
		let liste_tuile = []
		for(let y = 0; y < this.hauteur; y++) {
			for(let x = 0; x < this.largeur; x++) {
				liste_tuile.push(new Tuile(x, y))
				
			}
		}
		liste_tuile_temp.forEach((tuile) => {
			if(tuile.y < this.hauteur && tuile.x < this.largeur) {
				let indice_reel = Number(tuile.y)*this.largeur+Number(tuile.x)
				liste_tuile[indice_reel] = tuile
			}
		})
		return liste_tuile
	}
}