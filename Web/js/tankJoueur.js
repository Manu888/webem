/*window.onload = function() {
	canvas = document.getElementById("tank1")
	let couleur = [1.0, 0.2, 0.3]
	let tank_joueur = new TankJoueur(canvas, couleur)
	tank_joueur.tick()
 }*/
 

class TankJoueur {
	constructor(canvas, couleur, perdant=false) {
		this.gl = this.initialiser_gl(canvas)
		this.matrice_projection = mat4.perspective(45, this.gl.largeur/this.gl.hauteur, 0.1, 100.0, mat4.create())
		this.tank = new Tank3D(this.gl)
		this.ciel = new Ciel3D(this.gl)
		this.couleur = couleur
		this.perdant = perdant
		this.position_camera = null
	}

	tick() {
		window.requestAnimationFrame(()=>{this.tick()})
		this.afficher()
	}

	calculer_matrice_vue() {
		// calcul de l'angle de rotation
		let angle = Math.acos(vec3.dot([1, 0, 0], this.position_camera)/vec3.length(this.position_camera))
		if(this.position_camera[2] < 0) {
			angle = Math.PI+(Math.PI-angle);
		}
		angle += Math.PI/2
		
		// création de la matrice
		let matrice_vue = mat4.create()
		mat4.identity(matrice_vue)
		mat4.rotate(matrice_vue, angle, [0.0, 1.0, 0.0])
		mat4.translate(matrice_vue, this.position_camera)

		return matrice_vue
	}

	deplacer_camera() {
		let temps = new Date().getTime()

		let y = 0

		let x = Math.cos(temps/500)*TankJoueur.camera.distance
		let z = Math.sin(temps/500)*TankJoueur.camera.distance

		this.position_camera = [x, y, z]
	}

	afficher() {
		let gl = this.gl
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
		gl.clearColor(0.0, 0.0, 0.0, 1.0)

		let matrice_vue = mat4.create()
		mat4.identity(matrice_vue)


		this.deplacer_camera()
		matrice_vue = this.calculer_matrice_vue()
		if(this.perdant) {
			this.tank.afficher(this.matrice_projection, matrice_vue, [0.0, 0.0, 0.0], 0, 0, Math.PI/2, this.couleur)
		}
		else {
			this.tank.afficher(this.matrice_projection, matrice_vue, [0.0, -0.2, 0.0], 0, 0, 4.0, this.couleur)
		}
		
		this.ciel.afficher(this.matrice_projection, matrice_vue, this.perdant)
	}

	initialiser_gl(canvas) {
		let gl = canvas.getContext("webgl")
		// Si échec, réessaie avec le contexte "experimental-webgl"
		// On est probablement sur Edge.
		if(!gl) {
			gl = canvas.getContext("experimental-webgl")
		}
		gl.largeur = canvas.width
		gl.hauteur = canvas.height
		gl.viewport(0, 0, gl.largeur, gl.hauteur)
		gl.enable(gl.DEPTH_TEST)

		return gl
	}
}

TankJoueur.camera = {
	"distance": 3.6,
	"position": [0.0, 0.0, -3.5]
}
