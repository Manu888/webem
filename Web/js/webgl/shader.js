class Shader{
	constructor(gl, code_shader) {
		this.gl = gl
		this.programme = gl.createProgram()
		this.vertex_shader = this.obtenir_shader(code_shader.vertex, gl.VERTEX_SHADER)
		this.fragment_shader = this.obtenir_shader(code_shader.fragment, gl.FRAGMENT_SHADER)
		this.initialiser(code_shader.liste_attribute, code_shader.liste_uniform)
	}

	obtenir_shader(code, type) {
		let gl = this.gl // pour faciliter la lecture
		
		let shader = gl.createShader(type)

		gl.shaderSource(shader, code)
		gl.compileShader(shader)

		// gestion d'erreurs. Copié-collé de learningwebgl.com
		if(!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
			alert(gl.getShaderInfoLog(shader))
			return null
		}

		return shader
	}

	initialiser(liste_attribute, liste_uniform) {
		let gl = this.gl // pour faciliter la lecture

		gl.attachShader(this.programme, this.vertex_shader)
		gl.attachShader(this.programme, this.fragment_shader)
		gl.linkProgram(this.programme)

		// gestion d'erreurs. Copié-collé de learningwebgl.com
		if (!gl.getProgramParameter(this.programme, gl.LINK_STATUS)) {
			alert("Could not initialise shaders")
		}

		this.utiliser()

		liste_attribute.forEach((attribute) => {
			this.initialiser_attribute(attribute)
		})

		liste_uniform.forEach((uniform) => {
			this.initialiser_uniform(uniform)
		})
	}

	initialiser_attribute(attribute) {
		let gl = this.gl // pour faciliter la lecture

		this[attribute] = gl.getAttribLocation(this.programme, attribute)
		gl.enableVertexAttribArray(this[attribute])
	}

	initialiser_uniform(uniform) {
		let gl = this.gl // pour faciliter la lecture

		this[uniform] = gl.getUniformLocation(this.programme, uniform)
	}

	utiliser() {
		this.gl.useProgram(this.programme)
	}

	set_3fv(nom, valeur) {
		this.utiliser()
		this.gl.uniform3fv(this[nom], valeur)
	}
}
