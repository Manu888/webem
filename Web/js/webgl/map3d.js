class Map3D {
	constructor(canvas) {
		this.gl = this.initialiser_gl(canvas)
		this.arbre3d = new Arbre3D(this.gl)
		this.balle3d = new Balle3D(this.gl)
		this.ciel3d = new Ciel3D(this.gl)
		this.item3d = new Item3D(this.gl)
		this.mur3d = new Mur3D(this.gl)
		this.tank3d = new Tank3D(this.gl)
		this.matrice_projection = mat4.perspective(45, this.gl.largeur/this.gl.hauteur, 0.1, 100.0, mat4.create())
		this.matrice_vue = null

		this.camera_position = Map3D.camera.position

		this.hauteur_mur = 2
		this.hauteur_mur_mobile = this.hauteur_mur

		this.orientation_item = 0

	}

	deplacer_camera(liste_tank) {
		let position = vec3.create()
		liste_tank.forEach((tank) => {
			position[0] += tank.x
			position[1] += tank.y
			position[2] += tank.z
		})

		let qte_tank = liste_tank.length
		position[0] /= qte_tank
		position[1] /= qte_tank
		position[2] /= qte_tank
		
		position[0] = Map3D.camera.position[0]-position[0]
		position[1] = Map3D.camera.position[1]-	position[1]
		position[2] = Map3D.camera.position[2]
		this.camera_position = position
	}

	calculer_matrice_vue() {
		let matrice_vue = mat4.create()
		mat4.identity(matrice_vue)
		mat4.rotate(matrice_vue, Map3D.camera.angle_rotation, Map3D.camera.axe_rotation)
		mat4.translate(matrice_vue, this.camera_position)
		
		return matrice_vue
	}

	initialiser_gl(canvas) {
		let gl = canvas.getContext("webgl")

		// Si échec, on prend réessaie avec le contexte "experimental-webgl"
		// On est probablement sur Edge.
		if(!gl) {
			gl = canvas.getContext("experimental-webgl")
		}

		gl.largeur = canvas.width
		gl.hauteur = canvas.height
		gl.viewport(0, 0, gl.largeur, gl.hauteur)
		gl.enable(gl.DEPTH_TEST)

		return gl
	}

	afficher(map, enregistrement_ponctuel) {
		let gl = this.gl
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
		gl.clearColor(0.0, 0.0, 0.0, 1.0)

		this.deplacer_camera(enregistrement_ponctuel.liste_enr_tank)
		this.matrice_vue = this.calculer_matrice_vue()

		this.deplacer_camera(enregistrement_ponctuel.liste_enr_tank)

		this.afficher_liste_tuile(map)
		this.afficher_liste_tank(enregistrement_ponctuel.liste_enr_tank)
		this.afficher_liste_balle(enregistrement_ponctuel.liste_enr_balle)
		this.afficher_liste_item(enregistrement_ponctuel.liste_enr_item)

		this.ciel3d.afficher(this.matrice_projection, this.matrice_vue)

		this.hauteur_mur_mobile = enregistrement_ponctuel.hauteur_mur
	}

	afficher_liste_tank(liste_enr_tank) {
		liste_enr_tank.forEach((tank) => {
			let position = [tank.x, tank.y, tank.z]
			this.tank3d.afficher(this.matrice_projection, this.matrice_vue, position, tank.h, tank.p, tank.r, tank.couleur)
		})
	}

	afficher_liste_balle(liste_enr_balle) {
		liste_enr_balle.forEach((balle) => {
			let position = [balle.x, balle.y, balle.z]
			this.balle3d.afficher(this.matrice_projection, this.matrice_vue, position)
		})
	}

	afficher_liste_item(liste_enr_item) {
		liste_enr_item.forEach((item) => {
			let position = [item.x, item.y, item.z]
			this.orientation_item += 0.12
			position[2] += Math.sin(this.orientation_item*2)/3 // La hauteur est déterminée par l'orientation. Pourquoi pas...
			this.item3d.afficher(this.matrice_projection, this.matrice_vue, position, item.type, this.orientation_item)
		})
	}

	afficher_liste_tuile(map) {
		let offset_x = map.largeur-1
		let offset_y = map.hauteur-1.75

		map.liste_tuile.forEach((tuile) => {
			let offset_z = 0

			if(tuile.type == Tuile.correspondance_type["plancher"] ) {
				offset_z = -this.hauteur_mur
			}
			else if(tuile.type == Tuile.correspondance_type["mobile"]) {
				offset_z = this.hauteur_mur_mobile
			}
			else if(tuile.type == Tuile.correspondance_type["mobile inversé"]) {
				offset_z = -this.hauteur_mur_mobile-this.hauteur_mur
			}

			let position = [tuile.x*2-offset_x, tuile.y*2-offset_y, offset_z]

			this.mur3d.afficher(this.matrice_projection, this.matrice_vue, position)

			if(tuile.arbre==1) {
				position[2] += 1.0
				this.arbre3d.afficher(this.matrice_projection, this.matrice_vue, position)
			}
		})
	}
}

Map3D.camera = {
	position: [0, 16, -21],
	axe_rotation: [1, 0, 0],
	angle_rotation: -Math.PI/5
}