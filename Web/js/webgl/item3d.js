class Item3D extends Objet3D {
	constructor(gl) {
		super(gl, Item3D.code_shader, Item3D.modele)
	}

	afficher(matrice_projection, matrice_vue, position, type_item, orientation) {
		this.texture_courante = type_item

		let matrice_modele = this.obtenir_matrice_modele(position, orientation)
		super.preparer_shader(matrice_projection, matrice_vue, matrice_modele)
		super.afficher()
	}

	obtenir_matrice_modele(position, orientation) {
		let matrice_modele = mat4.create()
		mat4.identity(matrice_modele)
		mat4.translate(matrice_modele, position)
		mat4.rotate(matrice_modele, orientation, [0.0, 0.0, 1.0])
		mat4.scale(matrice_modele, [0.8, 0.8, 0.8])

		return matrice_modele
	}
}

// Généré à partir de item.egg
Item3D.modele = {
	indice: {
		taille_item: 1,
		qte_item: 36,
		liste_coord: [0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23]
	},
	sommet: {
		taille_item: 3,
		qte_item: 24,
		liste_coord: [0.587835, 0.587835, 0.258079, 0.587835, -0.587835, 0.258079, -0.587835, -0.587835, 0.258079, -0.587835, 0.587835, 0.258079, 0.395427, 0.395427, 1.59163, -0.395427, 0.395427, 1.59163, -0.395427, -0.395427, 1.59163, 0.395427, -0.395428, 1.59163, 0.587835, 0.587835, 0.258079, 0.395427, 0.395427, 1.59163, 0.395427, -0.395428, 1.59163, 0.587835, -0.587835, 0.258079, 0.587835, -0.587835, 0.258079, 0.395427, -0.395428, 1.59163, -0.395427, -0.395427, 1.59163, -0.587835, -0.587835, 0.258079, -0.587835, -0.587835, 0.258079, -0.395427, -0.395427, 1.59163, -0.395427, 0.395427, 1.59163, -0.587835, 0.587835, 0.258079, 0.395427, 0.395427, 1.59163, 0.587835, 0.587835, 0.258079, -0.587835, 0.587835, 0.258079, -0.395427, 0.395427, 1.59163]
	},
	texture: {
		liste: {
		"mitraillette":"texture/Mitraillette.png",
		"grenade":"texture/Grenade.png",
		"shotgun":"texture/Shotgun.png",
		"piege":"texture/Piege.png",
		"guide":"texture/Homing.png",
		"spring":"texture/Spring.png"},
		taille_item: 2,
		qte_item: 24,
		liste_coord: [1.002001, 0.002009, 0.997991, 1.002001, -0.002001, 0.997991, 0.002009, -0.002001, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.996418, -0.003557, 1.003557, 0.996418, 0.003582, 1.003557, -0.003557, 0.003582, 1.000596, 0.000597, 0.999403, 1.000596, -0.000596, 0.999403, 0.000597, -0.000596, 0.999764, -0.000236, 1.000236, 0.999764, 0.000236, 1.000236, -0.000236, 0.000236, -0.000118, 0.999882, 0.000118, -0.000118, 1.000118, 0.000118, 0.999882, 1.000118]
	}
}

Item3D.code_shader = {
	vertex: `attribute vec3 a_position;
			attribute vec2 a_texture_coord;

			uniform mat4 u_modele;
			uniform mat4 u_vue;
			uniform mat4 u_projection;

			varying vec2 v_texture_coord;

			void main(void) {
				gl_Position = u_projection*u_vue*u_modele*vec4(a_position, 1.0);
				v_texture_coord = a_texture_coord;
			}`,

	fragment: `precision mediump float;

			varying vec2 v_texture_coord;

			uniform sampler2D u_texture;

			void main(void) {
				vec4 couleur = texture2D(u_texture, vec2(v_texture_coord.s, v_texture_coord.t));
				if(couleur.a < 0.1)
					discard;
				gl_FragColor = vec4(couleur.rgb, couleur.a);
			}`,
	liste_attribute: ["a_position", "a_texture_coord"],
	liste_uniform: ["u_projection", "u_vue", "u_modele"]
}
