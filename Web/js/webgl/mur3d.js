class Mur3D extends Objet3D {
	constructor(gl) {
		super(gl, Mur3D.code_shader, Mur3D.modele)
	}

	afficher(matrice_projection, matrice_vue, position) {
		let matrice_modele = this.obtenir_matrice_modele(position)
		super.preparer_shader(matrice_projection, matrice_vue, matrice_modele)
		super.afficher()
	}

	obtenir_matrice_modele(position) {
		let matrice_modele = mat4.create()
		mat4.identity(matrice_modele)
		mat4.translate(matrice_modele, position)

		return matrice_modele
	}
}

Mur3D.modele = {
	indice: {
		taille_item: 1,
		qte_item: 36,
		liste_coord: [ // Copié-collé de http://learningwebgl.com/lessons/lesson07/index.html
									0, 1, 2,      0, 2, 3,    // Front face
									4, 5, 6,      4, 6, 7,    // Back face
									8, 9, 10,     8, 10, 11,  // Top face
									12, 13, 14,   12, 14, 15, // Bottom face
									16, 17, 18,   16, 18, 19, // Right face
									20, 21, 22,   20, 22, 23  // Left face
								]
	},
	sommet: {
		taille_item: 3,
		qte_item: 24,
		liste_coord: [ // Copié-collé modifié de http://learningwebgl.com/lessons/lesson07/index.html
									 // Front face
									 -1.0, -1.0,  1.0,
									 1.0, -1.0,  1.0,
									 1.0,  1.0,  1.0,
									-1.0,  1.0,  1.0,
						
									// Back face
									-1.0, -1.0, -1.0,
									-1.0,  1.0, -1.0,
									 1.0,  1.0, -1.0,
									 1.0, -1.0, -1.0,
						
									// Top face
									-1.0,  1.0, -1.0,
									-1.0,  1.0,  1.0,
									 1.0,  1.0,  1.0,
									 1.0,  1.0, -1.0,
						
									// Bottom face
									-1.0, -1.0, -1.0,
									 1.0, -1.0, -1.0,
									 1.0, -1.0,  1.0,
									-1.0, -1.0,  1.0,
						
									// Right face
									 1.0, -1.0, -1.0,
									 1.0,  1.0, -1.0,
									 1.0,  1.0,  1.0,
									 1.0, -1.0,  1.0,
						
									// Left face
									-1.0, -1.0, -1.0,
									-1.0, -1.0,  1.0,
									-1.0,  1.0,  1.0,
									-1.0,  1.0, -1.0,
								]
	},
	texture: {
		liste: {
			//"mur": "texture/BrickGroundWall.png"
			"mur": "texture/BrickGroundWall2.png"
		},
		taille_item: 2,
		qte_item: 24,
		liste_coord: [0.0, 0.501953125,
								0.498046875, 0.501953125,
								0.498046875,  1.0,
								0.0,  1.0,

								0.0087890625, 0.0029296875,
								0.498046875, 0.0029296875,
								0.498046875,  0.50390625,
								0.0087890625,  0.50390625,

								0.0087890625, 0.0029296875,
								0.498046875, 0.0029296875,
								0.498046875,  0.50390625,
								0.0087890625,  0.50390625,

								0.0087890625, 0.0029296875,
								0.498046875, 0.0029296875,
								0.498046875,  0.50390625,
								0.0087890625,  0.50390625,

								0.0087890625, 0.0029296875,
								0.498046875, 0.0029296875,
								0.498046875,  0.50390625,
								0.0087890625,  0.50390625,

								0.0087890625, 0.0029296875,
								0.498046875, 0.0029296875,
								0.498046875,  0.50390625,
								0.0087890625,  0.50390625]
	}
}

Mur3D.code_shader = {
	vertex: `attribute vec3 a_position;
			attribute vec2 a_texture_coord;

			uniform mat4 u_modele;
			uniform mat4 u_vue;
			uniform mat4 u_projection;

			varying vec2 v_texture_coord;

			void main(void) {
				gl_Position = u_projection*u_vue*u_modele*vec4(a_position, 1.0);
				v_texture_coord = a_texture_coord;
			}`,

	fragment: `precision mediump float;

		varying vec2 v_texture_coord;

		uniform sampler2D u_texture;

		void main(void) {
			vec4 couleur = texture2D(u_texture, vec2(v_texture_coord.s, v_texture_coord.t));
			if(couleur.a < 0.1)
				discard;
			gl_FragColor = vec4(couleur.rgb, couleur.a);
		}`,
	liste_attribute: ["a_position", "a_texture_coord"],
	liste_uniform: ["u_projection", "u_vue", "u_modele"]
}
