class Ciel3D extends Objet3D {
	constructor(gl) {
		super(gl, Ciel3D.code_shader, Ciel3D.modele)
	}

	afficher(matrice_projection, matrice_vue_ori, sombre) {
		let matrice_modele = this.obtenir_matrice_modele()
		
		// On enlève la translation de matrice_vue
		// Il est important de ne pas modifier la matrice originale
		let matrice_vue = Object.assign(mat4.create(), matrice_vue_ori)
		for(let x = 0; x<4; x++) {
			for(let y = 0; y<4; y++) {
				if(x == 3 || y == 3) {
					let i = x*4+y
					matrice_vue[i] = 0
				}
			}
		}

		super.preparer_shader(matrice_projection, matrice_vue, matrice_modele)
		if(sombre) {
			this.shader.set_3fv("u_couleur", Ciel3D.couleur_sombre)
		}
		else {
			this.shader.set_3fv("u_couleur", [1,1,1])
		}
		this.gl.depthFunc(this.gl.LEQUAL)
		super.afficher()
		this.gl.depthFunc(this.gl.LESS)
	}

	obtenir_matrice_modele() {
		let matrice_modele = mat4.create()
		mat4.identity(matrice_modele)

		return matrice_modele
	}
}

// constantes
Ciel3D.couleur_sombre = [0.57, 0.12, 0.12]

// Généré à partir de item.egg
Ciel3D.modele = {
	indice: {
		taille_item: 1,
		qte_item: 36,
		liste_coord: [0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23]
	},
	sommet: {
		taille_item: 3,
		qte_item: 24,
		liste_coord: [-3.421947, -3.472098, -3.461295, -3.423787, 3.431489, -3.49976, 3.479667, 3.433649, -3.442052, 3.481505, -3.469939, -3.403587, -3.479668, -3.433648, 3.442052, 3.423787, -3.431489, 3.49976, 3.421948, 3.472097, 3.461295, -3.481504, 3.46994, 3.403587, -3.421947, -3.472098, -3.461295, -3.479668, -3.433648, 3.442052, -3.481504, 3.46994, 3.403587, -3.423787, 3.431489, -3.49976, -3.423787, 3.431489, -3.49976, -3.481504, 3.46994, 3.403587, 3.421948, 3.472097, 3.461295, 3.479667, 3.433649, -3.442052, 3.479667, 3.433649, -3.442052, 3.421948, 3.472097, 3.461295, 3.423787, -3.431489, 3.49976, 3.481505, -3.469939, -3.403587, -3.479668, -3.433648, 3.442052, -3.421947, -3.472098, -3.461295, 3.481505, -3.469939, -3.403587, 3.423787, -3.431489, 3.49976]
	},
	texture: {
		liste: {"ciel":"texture/ciel.jpg"},
		taille_item: 2,
		qte_item: 24,
		liste_coord: [0.500026, 0.25114425, 0.499974, 0.49876725, 0.250144, 0.498738, 0.250197, 0.25111574999999997, 0.749856, 0.2511735, 0.999685, 0.25120275, 0.999633, 0.498825, 0.749804, 0.49879575, 0.500026, 0.25114425, 0.749856, 0.2511735, 0.749804, 0.49879575, 0.499974, 0.49876725, 0.499974, 0.49876725, 0.499922, 0.7463895, 0.250092, 0.746361, 0.250144, 0.498738, 0.250144, 0.498738, 0.000315, 0.49870950000000003, 0.000367, 0.2510865, 0.250197, 0.25111574999999997, 0.500078, 0.003522, 0.500026, 0.25114425, 0.250197, 0.25111574999999997, 0.250248, 0.0034927499999999998]
	}
}

Ciel3D.code_shader = {
	vertex: `attribute vec3 a_position;
			attribute vec2 a_texture_coord;

			uniform mat4 u_modele;
			uniform mat4 u_vue;
			uniform mat4 u_projection;

			varying vec2 v_texture_coord;

			void main(void) {
				vec4 position = u_projection*u_vue*vec4(a_position, 1.0);
				gl_Position = position.xyww;
				v_texture_coord = a_texture_coord;
			}`,

	fragment: `precision mediump float;

			varying vec2 v_texture_coord;

			uniform sampler2D u_texture;
			uniform vec3 u_couleur;

			void main(void) {
				vec4 couleur = texture2D(u_texture, vec2(v_texture_coord.s, v_texture_coord.t));
				if(couleur.a < 0.1)
					discard;

				gl_FragColor = vec4(couleur.r*u_couleur[0], couleur.g*u_couleur[1], couleur.b*u_couleur[2], couleur.a);
			}`,
	liste_attribute: ["a_position", "a_texture_coord"],
	liste_uniform: ["u_projection", "u_vue", "u_modele", "u_couleur"],
}
