class Objet3D {
	constructor(gl, code_shader, modele) {
		this.gl = gl
		this.shader = new Shader(gl, code_shader)
		this.sommet_buffer = null
		this.indice_buffer = null
		this.texture_buffer = null
		this.texture = {}
		this.texture_courante = null

		this.initialiser_texture(modele.texture.liste)
		this.initialiser_buffer(modele)
	}

	initialiser_texture(liste_texture) {
		let gl = this.gl // Pour faciliter la lecture

		for(let nom_texture in liste_texture) {
			this.texture_courante = nom_texture
			this.texture[nom_texture] = gl.createTexture()
			let texture = this.texture[nom_texture] // pour faciliter la lecture
			texture.image = new Image()
			texture.image.src = liste_texture[nom_texture]
			texture.image.onload = () => {
				gl.bindTexture(gl.TEXTURE_2D, texture)
				gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true)
				gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image)
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR)
				gl.generateMipmap(gl.TEXTURE_2D)

				gl.bindTexture(gl.TEXTURE_2D, null)
			}
		}
	}

  initialiser_buffer(modele) {
		this.initialiser_buffer_sommet(modele.sommet)
		this.initialiser_buffer_indice(modele.indice)
		this.initialiser_buffer_texture(modele.texture)
	}

	initialiser_buffer_sommet(info_sommet) {
		let gl = this.gl // Pour faciliter la lecture

		this.sommet_buffer = gl.createBuffer()
		gl.bindBuffer(gl.ARRAY_BUFFER, this.sommet_buffer)
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(info_sommet.liste_coord), gl.STATIC_DRAW)
		this.sommet_buffer.taille_item = info_sommet.taille_item
		this.sommet_buffer.qte_item = info_sommet.qt_item
	}

	initialiser_buffer_indice(info_indice) {
		let gl = this.gl // Pour faciliter la lecture

		this.indice_buffer = gl.createBuffer()
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indice_buffer)
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(info_indice.liste_coord), gl.STATIC_DRAW)
		this.indice_buffer.taille_item = info_indice.taille_item
		this.indice_buffer.qte_item = info_indice.qte_item
	}

	initialiser_buffer_texture(info_texture)   {
		let gl = this.gl // Pour faciliter la lecture

		let buffer = gl.createBuffer()
		gl.bindBuffer(gl.ARRAY_BUFFER, buffer)
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(info_texture.liste_coord), gl.STATIC_DRAW)
		buffer.taille_item = info_texture.taille_item
		buffer.qte_item = info_texture.qte_item
		this.texture_buffer = buffer
	}

	preparer_shader(matrice_projection, matrice_vue, matrice_modele) {
		let gl = this.gl // Pour faciliter la lecture
		
		let shader = this.shader // Pour faciliter la lecture
		shader.utiliser()

		// Passage des matrices au shader
		gl.uniformMatrix4fv(shader.u_projection, false, matrice_projection)
		gl.uniformMatrix4fv(shader.u_vue, false, matrice_vue)
		gl.uniformMatrix4fv(shader.u_modele, false, matrice_modele)

		// Bindage des sommets et passage au shader
		gl.bindBuffer(gl.ARRAY_BUFFER, this.sommet_buffer)
		gl.vertexAttribPointer(shader.a_position, this.sommet_buffer.taille_item, gl.FLOAT, false, 0, 0)

		// Bindage de la texture et passage au shader
		gl.bindBuffer(gl.ARRAY_BUFFER, this.texture_buffer)
		gl.vertexAttribPointer(shader.a_texture_coord, this.texture_buffer.taille_item, gl.FLOAT, false, 0, 0)
		gl.activeTexture(gl.TEXTURE0)
		gl.bindTexture(gl.TEXTURE_2D, this.texture[this.texture_courante])
		gl.uniform1i(shader.u_texture, 0)
	}
	

	// Il est essentiel d'appeller la fonction this.preparershader avant d'afficher
	afficher() {
		let gl = this.gl // Pour faciliter la lecture

		// Bindage des indices puis affichage
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indice_buffer)
		gl.drawElements(gl.TRIANGLES, this.indice_buffer.qte_item, gl.UNSIGNED_SHORT, 0)
	}
}
