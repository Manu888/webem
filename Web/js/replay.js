window.onload = function() {
	try {
		// On tente d'extraire la numéro de la partie
		let get = (window.location.href).match(/noReplay=\d/)
		let noPartie = get[0].split("=")[1]
		let replay = new Replay(noPartie)
		replay.tick()
	}
	catch(err) {
		// En cas d'échec, on retourne à la page des dernières parties jouées.
		window.location.replace("lastPlayed.php")
	}
}

class Replay {
	constructor(noPartie) {
		this.map = null

		this.liste_enregistrement = null
		this.indice_enregistrement = 0
		this.en_lecture = true // Doit partir en lecture. Autrement problème d'affichage.
		
		this.temps_derniere_image = new Date().getTime()

		// Affichage de la map en 3d
		let canvas3d = document.getElementById("map3d")
		this.map3d = new Map3D(canvas3d)

		// Affichage des infos des joueurs
		let canvas2d = document.getElementById("infos_joueurs")
		this.info_joueur = new InfoJoueur(canvas2d)
		

		// bouton lecture
		this.bouton_lecture = document.getElementById("bouton_lecture")
		this.bouton_lecture.onclick = () => {

			if(this.en_lecture) {
				this.en_lecture = false
				this.bouton_lecture.textContent = "Jouer"
			}
			else {
				this.en_lecture = true
				this.bouton_lecture.textContent = "Pause"
			}
		}
		// slider
		this.slider = document.getElementById("slider")
		this.slider.onchange = () => {
			this.indice_enregistrement = Number(this.slider.value)
			if(this.map) {
				// On affiche l'image.
				// Il est ainsi possible de visionner le replay sur pause.
				let enregistrement_ponctuel = this.liste_enregistrement[this.indice_enregistrement]
				this.map3d.afficher(this.map, enregistrement_ponctuel)
			}
		}

		this.obtenir_donnee_replay(noPartie);
	}

	tick() {
		window.requestAnimationFrame(()=>{this.tick()})
		if(this.map && this.en_lecture) {
			if(this.indice_enregistrement < this.liste_enregistrement.length) {
				let temps_courant = new Date().getTime()
				let delta_temps = temps_courant-this.temps_derniere_image

				let enregistrement_ponctuel = this.liste_enregistrement[this.indice_enregistrement]
				let resolution = enregistrement_ponctuel.resolution

				if(resolution <= delta_temps) {
					// on ajuste le slider
					this.slider.value = this.indice_enregistrement
					// on affiche le replay
					this.map3d.afficher(this.map, enregistrement_ponctuel)
					// si l'indice est égal au nombre d'enregistrement, alors il s'agit du dernier enregistrement
					let fin_partie = (this.indice_enregistrement == this.liste_enregistrement.length-1) ? true : false
					// on affiche les infos des joueurs
					this.info_joueur.afficher(enregistrement_ponctuel.liste_enr_tank, fin_partie)
	
					this.indice_enregistrement += 1
					this.temps_derniere_image = temps_courant
				}
			}
		}
	}

	obtenir_donnee_replay(noPartie) {
		let instance = this // Pour pouvoir passer l'instance à ajax

		$.ajax({
			url: "ajax/ajaxReplay.php",
			type: "POST",
			data: {"noPartie":noPartie}
		})
		.done(function (data) {
			data = JSON.parse(data)

			instance.creer_map(data.map)
			instance.liste_enregistrement = data.liste_enregistrement
			instance.info_joueur.initialiser_info(data.liste_info_joueur, data.id_gagnant)
			instance.initialiser_slider(data.liste_enregistrement.length)
		})
	}

	creer_map(infos_map) {
		this.map = new Map(infos_map.largeur, infos_map.hauteur, infos_map.liste_tuile)
	}

	initialiser_slider(max) {
		this.slider.setAttribute("max",max)
		this.slider.setAttribute("value",0)
	}
}