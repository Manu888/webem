let tableau = null;
let tableauTank = [];
let tableauCanvas = [];
let iterateur = 0;

window.onload = () => {
	tableau = document.getElementById("tableaulastplayed");
	creerTanks();
	lastPlayed();
}

// Fonction Ajax qui va chercher les 5 dernières parties sur la BD
function lastPlayed(){
	$.ajax({
		url: "ajax/ajax-lastPlayed.php",
		type: "POST"
	})
	.done(function (data){
		var infos = JSON.parse(data);
		$(".ligne").remove(); // delete des parties déjà présentes à chaque refresh du Ajax
		idReplay = 5;
		infos.forEach( elem => {afficherLastPlayed(elem, idReplay--);}); // Appel de la fonction d'append d'une ligne en plus d'un ID pour la page de replay
		iterateur = 0;
		setTimeout(lastPlayed, 10000);
	})
}
// Création des canvas et des objets WebGL pour les 10 joueurs possibles - ceux-ci seront persistants pour ne pas devoir recréer les objets WebGL à chaque rafraichissement Ajax
function creerTanks(){
	for (let i = 0; i < 10; ++i){
		let canvasTank = document.createElement("canvas");
		canvasTank.style.width = "250px";
		canvasTank.style.height = "180px";
		let couleurTank = [0.0, 0.0, 0.0];
		let nouveauTank = new TankJoueur(canvasTank, couleurTank);
		tableauTank.push(nouveauTank);
		tableauCanvas.push(canvasTank);
		nouveauTank.tick();
	}
}

// fonction qui append chacune des dernieres parties au talbeau
function afficherLastPlayed(object, idReplay){
	let template = document.getElementById("partie-template");
	let newLine = document.createElement("tr");

	// on associe le joueur 1 et 2 au bon array qui provient de la bd
	if (object[0].ID === object[3].ID_JOUEUR){
		// indexJoueur 1 et 2
		iJoueur1 = 0;
		iJoueur2 = 1;
	}
	else {
		iJoueur1 = 1;
		iJoueur2 = 0;
	}
	// extraction des couleurs pour chaque joueur
	let rouge1 = parseFloat(object[iJoueur1].TANK_COLOR_ROUGE.replace(",", "."));
	let vert1 = parseFloat(object[iJoueur1].TANK_COLOR_VERT.replace(",", "."));
	let bleu1 = parseFloat(object[iJoueur1].TANK_COLOR_BLEU.replace(",", "."));
	let couleurTank1 = [rouge1, vert1, bleu1];
	
	let rouge2 = parseFloat(object[iJoueur2].TANK_COLOR_ROUGE.replace(",", "."));
	let vert2 = parseFloat(object[iJoueur2].TANK_COLOR_VERT.replace(",", "."));
	let bleu2 = parseFloat(object[iJoueur2].TANK_COLOR_BLEU.replace(",", "."));
	let couleurTank2 = [rouge2, vert2, bleu2];
	
	// remplissage du template
	newLine.setAttribute("class", "ligne");
	newLine.innerHTML = template.innerHTML;
	newLine.querySelector(".nomMap").innerHTML = object.NOM_MAP;

	let j1perdant;
	let j2perdant;
	if (object.ID_JOUEUR_GAGNANT === object[iJoueur1].ID){
		newLine.querySelector(".winner").innerHTML = object[iJoueur1].NOM_CALCULE;
		j1perdant = false;
		j2perdant = true;
	}
	else{
		newLine.querySelector(".winner").innerHTML = object[iJoueur2].NOM_CALCULE;
		j1perdant = true;
		j2perdant = false;
	}
		
	newLine.querySelector(".joueur1").innerHTML = object[iJoueur1].NOM_CALCULE;
	newLine.querySelector(".tankjoueur1").appendChild(tableauCanvas[iterateur]);
	tableauTank[iterateur].couleur = couleurTank1;
	tableauTank[iterateur].perdant = j1perdant;
	iterateur++;

	newLine.querySelector(".joueur2").innerHTML = object[iJoueur2].NOM_CALCULE;
	newLine.querySelector(".tankjoueur2").appendChild(tableauCanvas[iterateur]);
	tableauTank[iterateur].couleur = couleurTank2;
	tableauTank[iterateur].perdant = j2perdant;

	// bouton replay lié avec un ID spécifique pour la page replay
	newLine.querySelector(".boutonReplay").childNodes[0].onclick = function(){
		window.location.replace("replay.php?noReplay="+idReplay);
	}

	tableau.appendChild(newLine);
	iterateur++;
}

