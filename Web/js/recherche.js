function press(e){
    if (e.keyCode === 13) {
        recherche()
     }
}
let joueurs;
let noPage;
function recherche() {
    let username = document.getElementById("name").value;
    
    
        $.ajax({
            url: "ajax/ajaxRecherche.php",
            type: "GET",
            data: {"name":username}
        })
        .done(function (data){
            joueurs = JSON.parse(data);
            let listeRecherche = document.getElementById("resultatRecherche");
            // On efface la liste de recherche précédante afin de laisser place à la nouvelle
            while (listeRecherche.firstChild) {
   			 listeRecherche.removeChild(listeRecherche.firstChild);
            }
            noPage = 1;
            let template = document.getElementById("player-template");
            if(joueurs.length >0){
                // Pour chacun des 5 premiers joueurs dans la liste, on récupere le template et on associe les champs aux infos
                for(let i = 0; i<5;i++){
                    let node = document.createElement("div");
                    let joueur = joueurs[i];
                    node.innerHTML = template.innerHTML;
                    node.querySelector(".username").innerHTML = joueur.USERNAME;
                    node.querySelector(".level").innerHTML = joueur.NIVEAU;
                    node.querySelector(".gamePlayed").innerHTML = joueur.NB_PARTIES_JOUEES;
                    node.querySelector(".buttonFiche").onclick = function() { window.location.assign("http://localhost/webem_all/Web/fiche.php?username="+joueur.USERNAME); };
                    if(joueur.NB_PARTIES_JOUEES>0){
                        // Calcul de ratio de victoire en pourcentage
                    node.querySelector(".winRate").innerHTML = Math.round((joueur.NB_PARTIES_GAGNEES / joueur.NB_PARTIES_JOUEES)*100).toString() + "%";
                    }
                    else{
                        node.querySelector(".winRate").innerHTML = "0%";
                    }
                    listeRecherche.appendChild(node);
                    ;
                }

                let pagination = document.getElementById("pagination");
                let nombrePage = Math.ceil(joueurs.length /5);
                // On enleve les chiffres de pages précédants et on remet les nouveaux
                while (pagination.firstChild) {
                    pagination.removeChild(pagination.firstChild);
                }
                // On suit la structure de BootStrap pour faire les li et les a à l'intérieur
                for(let k = 0; k<nombrePage ; k++){
                    let node = document.createElement("li");
                    node.className ="page-item";
                    let reference = document.createElement("a");
                    reference.className = "page-link";
                    reference.href = "#";
                    // Lorsqu'on clique sur un numéro on associe la fonction setNoPage
                    reference.onclick = setNopage;
                
                    let no = document.createTextNode(k+1);
                    reference.appendChild(no);
                    node.appendChild(reference);
                    
                    pagination.appendChild(node);
                }
            
            }
            else{
                // Si on a trouvé aucun joueur dans la BD correspondant, on affiche un avertissement
                let intro = document.createElement("h2");
                let textintro = document.createTextNode("Aucun joueur ne possède un username débutant par " + username);
                intro.appendChild(textintro);
                listeRecherche.appendChild(intro);
            }
        })

        function setNopage(){
            // Lorsqu'on clique sur le numero de page, on change la valeur à l'interne
            // puis on affiche la page correspondante au choix
            noPage = this.innerHTML;
            afficherPage()
        }

        function afficherPage(){
            let listeRecherche = document.getElementById("resultatRecherche");
            // On enlève tout les éléments de la liste de recherche afin de les
            // remplacer par ceux de la page correspondante
            while (listeRecherche.firstChild) {
                listeRecherche.removeChild(listeRecherche.firstChild);
            }

            let template = document.getElementById("player-template");
            let nombreAffiche;
            // On décale la valeur de la personne à afficher de sorte
            // qu'on affiche 5 personnes par pages
            if(noPage>1){
                nombreAffiche = (noPage-1)*5;
            }
            else{
                nombreAffiche = 0;
            }
            // On affiche 5 joueurs dans la liste de recherche
            for(let i = 0; i<5;i++){
                let node = document.createElement("div");
                let joueur = joueurs[i+nombreAffiche];
                // si on a un joueur, on l'associe au template
                if(joueur){
                    node.innerHTML = template.innerHTML;
                    node.querySelector(".username").innerHTML = joueur.USERNAME;
                    node.querySelector(".level").innerHTML = joueur.NIVEAU;
                    node.querySelector(".gamePlayed").innerHTML = joueur.NB_PARTIES_JOUEES;
                    node.querySelector(".buttonFiche").onclick = function() { window.location.assign("http://localhost/webem_all/Web/fiche.php?username="+joueur.USERNAME); };
                    if(joueur.NB_PARTIES_JOUEES>0){
                    node.querySelector(".winRate").innerHTML = Math.round((joueur.NB_PARTIES_GAGNEES / joueur.NB_PARTIES_JOUEES)*100).toString() + "%";
                    }
                    else{
                        node.querySelector(".winRate").innerHTML = "0%";
                    }
                    listeRecherche.appendChild(node);
                }
            }
        
    }  
}