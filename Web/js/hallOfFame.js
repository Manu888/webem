let tableau = null;
let tableauTank = [];
let tableauCanvas = [];
let iterateur = 0;

window.onload = () => {
	tableau = document.getElementById("tableau-hall");
	creerTanks();
	getHallOfFame();
}

// Fonction ajax qui va chercher les membres du hall of fame et les passes à la fonction de création de ligne du talbeau
function getHallOfFame(){
	$.ajax({
		url: "ajax/ajaxHallOfFame.php",
		type: "POST"
	})
	.done(function (retour) {
		let liste = JSON.parse(retour);
		$(".ligne").remove(); // delete toutes les lignes du tableau
		//
		for (let i = 0; i < liste.length; i++){
			ajouterJoueur(liste[i], i + 1) // appel de la fonction qui append les lignes au tableau
		}
		iterateur = 0;
		setTimeout(getHallOfFame, 10000); // rappel dans 10s
	})
}

// Création des canvas et des objets WebGL pour les 10 joueurs possibles - ceux-ci seront persistants pour ne pas devoir recréer les objets WebGL à chaque rafraichissement Ajax
function creerTanks(){
	for (let i = 0; i < 10; ++i){
		let canvasTank = document.createElement("canvas");
		canvasTank.style.width = "250px";
		canvasTank.style.height = "180px";
		let couleurTank = [0.0, 0.0, 0.0];
		let nouveauTank = new TankJoueur(canvasTank, couleurTank);
		tableauTank.push(nouveauTank);
		tableauCanvas.push(canvasTank);
		nouveauTank.tick(); // fonction qui fait tourner les tanks
	}
}

// fonction qui append une ligne au tableau du Hall of Fame selon le template présent
function ajouterJoueur(joueur, position){
	let template = document.getElementById("hall-template");
	let newLine = document.createElement("tr");
	let couleurTank = [];

	// extraction de la couleur du joueur
	if (joueur[0] != null){
		let rouge = parseFloat(joueur[0].TANK_COLOR_ROUGE.replace(",", "."));
		let vert = parseFloat(joueur[0].TANK_COLOR_VERT.replace(",", "."));
		let bleu = parseFloat(joueur[0].TANK_COLOR_BLEU.replace(",", "."));
		couleurTank = [rouge, vert, bleu];
	}

	newLine.setAttribute("class", "ligne"); // attribution d'une classe pour la suppression à la mise à jour
	newLine.innerHTML = template.innerHTML;
	newLine.querySelector(".positionHall").innerHTML = position;
	newLine.querySelector(".nomJoueur").innerHTML = joueur.USERNAME;
	newLine.querySelector(".imageRang").innerHTML = "<img src=images/rank"+(position-1)+".png>"; // badge de position du Hall of Fame
	if (joueur[1] == undefined){
		newLine.querySelector(".niveauFavori").innerHTML = " -- ";
	}
	else {
		newLine.querySelector(".niveauFavori").innerHTML = joueur[1].NOM;
	}
	// ajout du canvas du tank avec la couleur du joueur en question
	newLine.querySelector(".tank").appendChild(tableauCanvas[iterateur]);
	tableauTank[iterateur].couleur = couleurTank;

	// arrondissement du winrate
	joueur.WINRATE = joueur.WINRATE.replace(",",".");
	newLine.querySelector(".ratio").innerHTML = Math.round(parseFloat(joueur.WINRATE)*100) + "%";
	newLine.querySelector(".nbParties").innerHTML = joueur[0].NB_PARTIES_JOUEES;
	
	tableau.appendChild(newLine);
	iterateur++;
}