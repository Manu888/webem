let critere = false;
function checkSiDispo(type) {
	let valeur = null;
	let node = null;
	if(type == "username"){
		valeur = document.getElementById("username").value;
		node = "validateUsername";
	
	}
	else if(type == "email"){
		valeur = document.getElementById("courriel").value;
		node = "validateEmail";
		
	}

	if(valeur != ""){
		$.ajax({
			url: "ajax/ajaxEnregistrement.php",
			type: "POST",
			data: {"type": type,
				"valeur":valeur}
		})
		.done(function (data){
			let data1 = JSON.parse(data)
			if(data1 == true){
				document.getElementById(node).innerHTML = type + " valide!";
				document.getElementById(node).style.color = "green";
				
			}
			else {
				document.getElementById(node).innerHTML = type + " déjà utilisé!";
				document.getElementById(node).style.color = "red";
				
			}
		})
	}
}

function checkPassword() {
	let password = document.getElementById("password").value;	
	if(password != ""){
		let pattern = /^(?=.*[0-9])(?=.*[!@#$%?*+-])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{9,30}$/;
		if(pattern.test(password)){
			document.getElementById("password").style.borderColor = "lightgreen";
			document.getElementById("passwordCheck").innerHTML = "Mot de passe valide!";
			document.getElementById("passwordCheck").style.color = "green";
		}
		else{
			document.getElementById("password").style.borderColor = "red";
			document.getElementById("password").value = "";
			document.getElementById("passwordCheck").innerHTML = "Le mot de passe ne respecte pas les critères de sécurité";
			document.getElementById("passwordCheck").style.color = "red";
		}	
	}
}

function confirmPassword() {
	let initialPass = document.getElementById("password").value;
	let confirmationPass = document.getElementById("confirmpassword").value;

	if(confirmationPass != ""){
		if(initialPass != confirmationPass){
			document.getElementById("confirmpassword").style.borderColor = "red";
			document.getElementById("confirmpassword").value = "";
			document.getElementById("confirmPasswordCheck").innerHTML = "Les mots de passe ne correspondent pas";
			document.getElementById("confirmPasswordCheck").style.color = "red";
		}
		else {
			document.getElementById("confirmpassword").style.borderColor = "lightgreen";
			document.getElementById("confirmPasswordCheck").innerHTML = "Mot de passe confirmé!";
			document.getElementById("confirmPasswordCheck").style.color = "green";
		}
	}
}

function showCriteres(){
	$(function() {$("#criteres").slideToggle()})
}

function changePassword(){
	$(function() {$("#modifPassword").slideToggle()})
}