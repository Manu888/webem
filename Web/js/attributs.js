let vieBD = null;
let vitesseBD = null;
let modif_attr = false;
let points_dispo_depart = null;
let vie_depart = null;
let force_depart = null;
let agilete_depart = null;
let dexterite_depart = null;

let maxAttr = 30;

window.onload = () => {
    getInfos(); // appel de la fonction ajax qui fetch les attributs du joueur
    // Le onclick pour les boutons plus
    $(".bouton-plus").click(function (){
        // vérification que le bouton est activé
        if (modif_attr){
            // vérification qu'il reste des points à dépenser
            if (parseInt($("#pointsDispo").html()) >= 1 ){
                // pour chaque bouton on vérifie si on est déjà à la limite de points
                if (this.id === "plus-vie" && parseInt($("#attr-vie").html()) < maxAttr){
                    $("#attr-vie").html( parseInt($("#attr-vie").html()) + 1 );
                    $("#pointsDispo").html( parseInt($("#pointsDispo").html()) - 1 );
                }
                else if (this.id === "plus-for" && parseInt($("#attr-for").html()) < maxAttr){
                    $("#attr-for").html( parseInt($("#attr-for").html()) + 1 );
                    $("#pointsDispo").html( parseInt($("#pointsDispo").html()) - 1 );
                }
                else if (this.id === "plus-agi" && parseInt($("#attr-agi").html()) < maxAttr){
                    $("#attr-agi").html( parseInt($("#attr-agi").html()) + 1 );
                    $("#pointsDispo").html( parseInt($("#pointsDispo").html()) - 1 );
                }
                else if (this.id === "plus-dex" && parseInt($("#attr-dex").html()) < maxAttr){
                    $("#attr-dex").html( parseInt($("#attr-dex").html()) + 1 );
                    $("#pointsDispo").html( parseInt($("#pointsDispo").html()) - 1 );
                }
                calculerBonus(); // appel de la fonction de recalcul des bonus
            }
        }
        
    });

    // Le onclick pour les boutons plus
    $(".bouton-moins").click(function (){
        // vérification que le bouton est activé
        if (modif_attr){
            // vérification qu'on ne remonte pas plus haut que les points disponibles au départ
            if (parseInt($("#pointsDispo").html()) < points_dispo_depart ){
                // pour chaque bouton qu'on ne descend pas plus bas que les attributs déjà enregistrés
                if (this.id === "moins-vie" && parseInt($("#attr-vie").html()) > vie_depart){
                    $("#attr-vie").html( parseInt($("#attr-vie").html()) - 1 );
                    $("#pointsDispo").html( parseInt($("#pointsDispo").html()) + 1 );
                }
                else if (this.id === "moins-for" && parseInt($("#attr-for").html()) > force_depart){
                    $("#attr-for").html( parseInt($("#attr-for").html()) - 1 );
                    $("#pointsDispo").html( parseInt($("#pointsDispo").html()) + 1 );
                }
                else if (this.id === "moins-agi" && parseInt($("#attr-agi").html()) > agilete_depart){
                    $("#attr-agi").html( parseInt($("#attr-agi").html()) - 1 );
                    $("#pointsDispo").html( parseInt($("#pointsDispo").html()) + 1 );
                }
                else if (this.id === "moins-dex" && parseInt($("#attr-dex").html()) > dexterite_depart){
                    $("#attr-dex").html( parseInt($("#attr-dex").html()) - 1 );
                    $("#pointsDispo").html( parseInt($("#pointsDispo").html()) + 1 );
                }
                calculerBonus(); // appel de la fonction de recalcul des bonus
            }
        }
    });

    // on click du bouton Sauvegarder
    $("#confirm").click(function (){
        // vérification que le bouton est activé
        if (modif_attr){
            // appel de la fonction ajax qui lit les valeurs et les envoi a la BD
            updateAttributs();
        }
    });

    // on click du bouton Annuler
    $("#annuler").click(function (){
        // vérification que le bouton est activé
        if (modif_attr){
            // reload les valeurs de la BD
            getInfos();
        }
    });
}

// Fonction Ajax qui va chercher les attributs et points du joueur et les affiche
function getInfos(){
    $.ajax({
        url: "ajax/ajaxAttributs.php",
        type: "POST",
        data: {"cmd": "fetch"}
    })
    .done(function (retour){
        // mise en place des valeurs initiales pour éviter de réduire ses points déjà sauvegardés
        let infos = JSON.parse(retour);
        vieBD = parseFloat(infos[1].VALEUR);
        vitesseBD = parseFloat(infos[2].VALEUR);
        points_dispo_depart = parseInt(infos[0].POINTS_ATTR);
        vie_depart = infos[0].ATTR_VIE;
        force_depart = infos[0].ATTR_FORCE;
        agilete_depart = infos[0].ATTR_AGILETE;
        dexterite_depart = infos[0].ATTR_DEXTERITE;

        // affichage des points
        $("#pointsDispo").html(points_dispo_depart);
        $("#attr-vie").html(vie_depart);
        $("#attr-for").html(force_depart);
        $("#attr-agi").html(agilete_depart);
        $("#attr-dex").html(dexterite_depart);
        calculerBonus(); // calcul des bonus en fonction des points actuels

        // boolean qui désactive les boutons s'il n'y a pas de points à dépenser
        if(points_dispo_depart > 0)
            modif_attr = true; 
    })
}

// Fonction Ajax qui envoi les nouveaux attributs à la BD
function updateAttributs(){
    // remplissage du tableau des infos requises par la BD
    let tableauDTO = [];
    tableauDTO.push(parseInt($("#attr-vie").html()));
    tableauDTO.push(parseInt($("#attr-for").html()));
    tableauDTO.push(parseInt($("#attr-agi").html()));
    tableauDTO.push(parseInt($("#attr-dex").html()));
    tableauDTO.push(parseInt($("#pointsDispo").html()));
    let dtoJoueur = JSON.stringify(tableauDTO); // tranformation en JSON pour le transfert javascript->php
    $.ajax({
        url: "ajax/ajaxAttributs.php",
        type: "POST",
        data: {
            "cmd": "update",
            "dto": dtoJoueur
        }
    })
    .done(function (retour){
        let reponse = JSON.parse(retour);
        // vérification si la sauvegarde a été complétée
        if (reponse == "OK"){
            $("#message").html("Modification sauvegardée !");
            getInfos();
        }
        else {
            $("#message").html("Erreur de sauvegarde");
        }
    })
}

// Fonction qui recalcule et affiche les bonus en fonction des points d'aatributs dépensés
function calculerBonus(){
    $("#bonus-vie").html( Math.round(vieBD + (vieBD * parseInt($("#attr-vie").html()) * 0.1)) );
    $("#bonus-for").html( String($("#attr-for").html() * 10) + " %" );
    $("#bonus-agi").html( (vitesseBD + (vitesseBD *  $("#attr-agi").html() * 0.05)).toFixed(1) );
    $("#bonus-dex").html( String($("#attr-dex").html() * 10) + " %" );
}
