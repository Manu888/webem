<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();

	$wrongLogin = $action->wrongLogin;
	$userBlocked = $action->userBlocked;
	$tentatives = $action->tentatives;

	require_once("partial/header.php");
?>
<script src="js/index.js"></script>

<h1>Connexion</h1>

<div class="login-form-frame centralform">		
	<?php 
		if ($userBlocked) {
			?>
			<div class="error-div"><strong>TROP DE TENTATIVES MANQUÉES. COMPTE BLOQUÉ.</div>
			<?php
		}
		else if ($wrongLogin) {
			?>
			<div class="error-div"><strong>Erreur : </strong>Connexion erronée: <?=5 - $tentatives?> tentatives restantes.</div>
			<?php
		}
	?>
	<form action="index.php" method="post" > <!-- class="form-inline" -->
		<div class="form-group row">
			<label class="col-sm-4 col-form-label userlabel" for="username" >Username : </label>					
			<div class="col-sm-7">					
				<input class="form-control" type="text" class="form-control" name="username" id="username" placeholder="username">
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-4 col-form-label passwordlabel" for="password" >Mot de passe : </label>
			<div class="col-sm-7">					
				<input class="form-control" type="password" class="form-control" name="pwd" id="password" placeholder="Password">
				<p><a href="#" onclick="forgotEmail()">Mot de passe oublié?</a></p>
				<div id= "forgotPassword">
					<div class="form-label">
						<label for="email">Entrez votre courriel </label>
					</div>
					<div class="form-input" >
						<input type="text" name="email" id="email" />
						<p id="emailCheck"></p>
					</div>
					<button type="button" onclick="sendEmail()" class="btn btn-primary">M'envoyer mon mot de passe</button>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary">Se connecter</button>
	</form>
	
	<div class="creercompte">
		<a href="enregistrement.php">Nouvel utilisateur?
		<br>Enregistrez-vous ici</a>
	</div>
</div>
<?php
	require_once("partial/footer.php");
