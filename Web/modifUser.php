<?php
	require_once("action/ModifUserAction.php");

	$action = new ModifUserAction();
	$action->execute();
	
	$joueur = $action->joueur;
	require_once("partial/header.php");
?>
<script src="js/enregistrement.js"></script>
<div class="enregistrement-form-frame">
	<h2>Modification de profil</h2>
		<form action="modifUser.php" method="post" id="formLayout">

			<div class="form-label">
				<label for="prenom">Prenom : </label>
			</div>
			<div class="form-input">
				<input type="text" required name="prenom" id="prenom" value="<?=$joueur[0]["PRENOM"]?>" maxlength="20"/>
			</div>
			<div class="form-label">
				<label for="nom">Nom : </label>
			</div>
			<div class="form-input">
				<input type="text" required name="nom" id="nom" value="<?=$joueur[0]["NOM"]?>" maxlength="20"/>
			</div>
			<div class="form-label">
				<label for="username">Nom d'usager : </label>
			</div>
			<div class="form-input">
			<input type="text" onfocusout="checkSiDispo('username')" required name="username" id="username" value="<?=$joueur[0]["USERNAME"]?>" maxlength="20"/>
			</div>
			<p id="validateUsername"></p>
			<div class="form-separator"></div>

			<a href="#" onclick=changePassword()> Modifier votre mot de passe</a>
			<div id= "modifPassword">
				<div class="form-label">
					<label for="password">Nouveau mot de passe : </label>
				</div>
				<div class="form-input" >
					<input type="password" onfocusout="checkPassword()" name="password" id="password" />
					<div onclick="showCriteres()"> &#10068; Critères </div>
					<p id="criteres"><span>Le mot de passe doit avoir au moins 9 caractères,</br>dont au moins une lettre, un chiffre et </br>un caractère parmis ! , @ , # , $ , % , ? , * , + , et -</span></p>
					<p id="passwordCheck"></p>
				</div>
				<div class="form-label">
					<label for="password">Confirmer nouveau mot de passe : </label>
				</div>
				<div class="form-input">
					<input type="password" onfocusout="confirmPassword()" name="confirmpassword" id="confirmpassword" />
					<p id="confirmPasswordCheck"></p>
				</div>
			</div>

			<div class="form-label">
				<label for="courriel">Courriel : </label>
			</div>
			<div class="form-input">
				<input type="text" onfocusout="checkSiDispo('email')" name="courriel" id="courriel" value="<?=$joueur[0]["COURRIEL"]?>" maxlength="20"/>
				<p id="validateEmail"></p>
			</div>
			<div class="form-label">
				<label for="couleur">Couleur de votre tank : </label>
			</div>
			<div class="form-input">
				<input type="color" name="couleur" id="couleur" value="<?=$joueur[0]["COULEUR"]?>"/>
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				&nbsp;
			</div>
			<div class="form-label">
				<label for="password">Entrez votre mot de passe pour confirmer les modifications </label>
			</div>
			<div class="form-input" >
				<input type="password" required name="truePassword" id="truePassword" />
				<p id="passwordCheck"></p>
			</div>
			<div class="form-input">
				<button type="submit">Enregistrer les modifications</button>
			</div>
			<div class="form-separator"></div>
		</form>

<?php
	require_once("partial/footer.php");