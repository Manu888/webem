<?php
	require_once("../action/AjaxRechercheAction.php");

	$action = new AjaxRechercheAction();
	$action->execute();

	echo json_encode($action->result);