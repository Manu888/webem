<?php
    chdir("../.");
    require_once("action/AjaxAttributsAction.php");
    
    $action = new AjaxAttributsAction();
	$action->execute();

	echo json_encode($action->result);