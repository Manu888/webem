<?php
	chdir("../.");
	require_once("action/AjaxLastPlayedAction.php");

	$action = new AjaxLastPlayedAction();
	$action->execute();

	echo json_encode($action->result);