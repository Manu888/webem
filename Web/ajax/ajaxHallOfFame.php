<?php
	chdir("../.");
	require_once("action/AjaxHallOfFameAction.php");

	$action = new AjaxHallOfFameAction();
	$action->execute();

	echo json_encode($action->result);