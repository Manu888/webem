<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Tankem All</title>
        <link href="css/global.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="js/util/jquery.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
		<?php
			if(isset($_SESSION["css_extra"])) {
				foreach($_SESSION["css_extra"] as $css) {
				?>
					<link href="<?=$css?>" rel="stylesheet" type="text/css"/>
				<?php
				}
			}
		?>
		<!-- pour se proteger du clickjacking -->
		<script type="text/javascript">if (top != self) top.location.replace(self.location.href);</script> 
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    </head>
	<header>
		<h1 class="mx-auto" style="width: 400px;">Tankem ALL !</h1>
	</header>
    <body id="body" class:"container-fluid full">
	<nav class="navbar navbar-expand-lg navbar-expand-sm navbar-light bg-light">
		<div  class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto"><?php
				if (!$action->isLoggedIn()) {
				?>
				<li class="nav-item active"><p id="connexion"><a class="nav-link" href="index.php">[Connexion]</a></p></li>
			<?php
				}
			?>
			<li class="nav-item active"><p><a class="nav-link" href="recherche.php">Rechercher Joueur</a></p> </li>
			<li class="nav-item active"><p><a class="nav-link" href="hallOfFame.php">Hall of Fame</a></p> </li>
			<li class="nav-item active"><p><a class="nav-link" href="lastPlayed.php">Dernières parties jouées</a></p> </li>
			
			<?php
				if ($action->isLoggedIn()) {
				?>
				<li class="nav-item active"><p><a class="nav-link" href="modifUser.php">Modifier votre profil</a></p></li>
				<li class="nav-item active"><p><a class="nav-link" href="attributsJoueur.php">Attributs de votre tank</a></p> </li>
				<li class="nav-item active"><p id="deconnexion"><a class="nav-link" href="?action=logout">[Déconnexion]</a></p></li>				
			<?php
				}
			?>
		</ul>
		</div>
	</nav>