 <?php
	require_once("action/AttributsJoueurAction.php");

	$action = new AttributsJoueurAction();
	$action->execute();

	require_once("partial/header.php");
?>
<script src="js/attributs.js"></script>
<div id="page-attributs">
 	<h1>Attributs</h1>
	<section>
 		<h4>Points disponibles</h4>
		<div id="pointsDispo"></div>
	</section>
	<section>
		<h4>Points d'attributs assignés</h4>
		<div class="conteneur-attribut">
			<div class="tag-attr">Vie : </div>
			<div class="attr" id="attr-vie"></div>
			<button class="bouton-plus" id="plus-vie">+</button>
			<button class="bouton-moins" id="moins-vie">-</button>
		</div>
		<div class="conteneur-attribut">
			<div class="tag-attr">Force : </div>
			<div class="attr" id="attr-for"></div>
			<button class="bouton-plus" id="plus-for">+</button>
			<button class="bouton-moins" id="moins-for">-</button>
		</div>
		<div class="conteneur-attribut">
			<div class="tag-attr">Agileté : </div>
			<div class="attr" id="attr-agi"></div>
			<button class="bouton-plus" id="plus-agi">+</button>
			<button class="bouton-moins" id="moins-agi">-</button>
		</div>
		<div class="conteneur-attribut">
			<div class="tag-attr">Dexterité : </div>
			<div class="attr" id="attr-dex"></div>
			<button class="bouton-plus" id="plus-dex">+</button>
			<button class="bouton-moins" id="moins-dex">-</button>
		<div class="conteneur-attribut">
		<div>
			<button id="confirm">Sauvegarder</button>
			<button id="annuler">Annuler</button>
			<div id="message"></div>
		</div>
	</section>
	<section id="stats-calculees">
		<h4>Stats calculées : </h4>
		<div class="conteneur-attribut">
			<div class="tag-bonus">hp calculé : </div>
			<div class="bonus" id="bonus-vie"></div>
		</div>
		<div class="conteneur-attribut">
			<div class="tag-bonus">bonus dégat : </div>
			<div class="bonus" id="bonus-for"></div>
		</div>
		<div class="conteneur-attribut">
			<div class="tag-bonus">vitesse déplacement : </div>
			<div class="bonus" id="bonus-agi"></div>
		</div>
		<div class="conteneur-attribut">
			<div class="tag-bonus">vitesse tirs : </div>
			<div class="bonus" id="bonus-dex"></div>
		</div>
	</section>
</div>
<?php
	require_once("partial/footer.php");