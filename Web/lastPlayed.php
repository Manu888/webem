<?php
	require_once("action/LastPlayedAction.php");

	$action = new LastPlayedAction();
	$action->execute();

	require_once("partial/header.php");
?>
<script src="js/lastPlayed.js"></script>
<script src="js/util/glMatrix-0.9.5.min.js"></script>
<script src="js/webgl/shader.js"></script>
<script src="js/webgl/objet3d.js"></script>
<script src="js/webgl/ciel3d.js"></script>
<script src="js/webgl/tank3d.js"></script>
<script src="js/tankJoueur.js"></script>
<script type="x-template" id="partie-template">
	<td class="nomMap"></td>
	<td class="joueur1"></td>
	<td class="tankjoueur1"></td>
	<td class="joueur2"></td>
	<td class="tankjoueur2"></td>
	<td class="winner"></td>
	<td class="boutonReplay"><button>Replay</button></td>
</script>
<div >
	<table id="tableaulastplayed">
		<tr>
			<th class="th">Nom du Tableau</th>
			<th class="th">Joueur 1</th>
			<th class="th">Tank Joueur 1</th>
			<th class="th">Joueur 2</th>
			<th class="th">Tank Joueur 2</th>
			<th class="th">Gagnant</th>
			<th class="th">Replays</th>
		</tr>
	</table>
</div>
<?php
	require_once("partial/footer.php");