<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/AttributsJoueurDAO.php");
    require_once("action/dao/InfoPlayerDAO.php");

    class AjaxAttributsAction extends CommonAction {
        protected $daoJoueur = null;

        public function __construct() {
            parent::__construct(CommonAction::$VISIBILITY_MEMBER);
        }
        protected function executeAction() {
            if ($_POST["cmd"] === "fetch"){
                $daoJoueur = new InfoPlayerDAO();
                $joueur = $daoJoueur->getInfoPlayer($_SESSION["username"]);
                $statsBase = AttributsJoueurDAO::getStatsBase();
                $this->result = array_merge($joueur, $statsBase);
            }
            else if ($_POST["cmd"] === "update"){
                $dtoJoueur = json_decode($_POST["dto"]);
                $dtoJoueur[] = $_SESSION["username"];
                $this->result = AttributsJoueurDAO::updateJoueur($dtoJoueur);
            }
            //return $this->result;
        }
    }