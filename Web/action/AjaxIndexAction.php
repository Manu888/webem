<?php
	require_once("CommonAction.php");
	require_once("dao/InfoPlayerDAO.php");

	class AjaxIndexAction extends CommonAction {

		public $result = null;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$this->result = null;
			$dao = new InfoPlayerDao();
			$email = $_POST["email"];
			$resultat = $dao->changePasswordFromEmail($email);
			if($this->result){
				$message = "Votre nouveau mot de passe est: SinisterS1x! . \n Veuillez le changer dès que possible!";
				// Envoi du mail
				mail($email, 'Nouveau Mot de passe pour TankEM', $message);
			}
			$this->result = json_encode($resultat);
		}
	}