<?php
	require_once("action/dao/Connection.php");
	require_once("action/dao/InfoPlayerDAO.php");

	class HallOfFameDAO {

		// fonction qui retourne les infos nécessaires du joueur à partir de la liste des meilleurs joueurs
		public static function listeHallOfFame(){	
			try{
				$connection = Connection::getConnection();
				
				$daoPlayer = new InfoPlayerDAO();
				$bestPlayers = $daoPlayer->getBestPlayers(); // liste des meilleurs joueurs
				$hallPlayers = [];

				// pour chaque meilleur joueur on va chercher ses infos
				foreach($bestPlayers as $bestPlayer){
					$tempInfoPlayer = $daoPlayer->getInfoPlayerID($bestPlayer["ID"]);
					$tempInfoMap = $daoPlayer->getPrefMap($bestPlayer["ID"]);
					$hallPlayers[] = array_merge($bestPlayer, $tempInfoPlayer, $tempInfoMap); // on merge les infos pour chaque joueur en un array
				}
				// on retourne les membres du hall et leurs infos
				return $hallPlayers;				
            }
			catch(PDOException $e){
				echo($e->getCode());
				echo($e->getMessage());
				return "ERREUR BD";
			}
		}
	}