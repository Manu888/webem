<?php
	require_once("action/dao/Connection.php");
	require_once("action/dao/InfoPlayerDAO.php");

	class LastPlayedDAO {

		public $result = [];

		// fonction qui retourne les 5 dernières parties et les infos pour chaque joueur participant
		public static function listeLastPlayed(){	
			try{
				$dao = new InfoPlayerDAO();
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT * FROM PARTIE WHERE ROWNUM <= 5 ORDER BY ID DESC");
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				
				$rows = $statement->fetchall();
				$retour = [];
				foreach ($rows as $row){
					$infoJoueur1 = $dao->getInfoPlayerID($row["ID_JOUEUR_GAGNANT"]);
					$infoJoueur2 = $dao->getInfoPlayerID($row["ID_JOUEUR_PERDANT"]);
					$nomMap = LastPlayedDAO::obtenirNomMap($row["ID_MAP"], $connection);
					$noJoueurs = LastPlayedDAO::getNoJoueurs($row["ID"]);
					$temp = array_merge($row, $infoJoueur1, $infoJoueur2, $nomMap, $noJoueurs);
					$retour[] = $temp;
				}
				return $retour;				
            }
			catch(PDOException $e){
				echo($e->getCode());
				echo($e->getMessage());
				return "ERREUR BD";
			}
		}

		// fonction qui va chercher le nom de la map selon son Id
		private static function obtenirNomMap($idMap, $connection) {
			$statement = $connection->prepare("SELECT NOM AS NOM_MAP FROM map WHERE id = ?");
			$statement->bindParam(1, $idMap);
			$statement->execute();
			$nomMap = $statement->fetch();
			return $nomMap;
		}

		private static function getNoJoueurs($idPartie){
			$connection = Connection::getConnection();
			$statement = $connection->prepare("SELECT ID_JOUEUR, NO_JOUEUR FROM enr_info_joueur WHERE ID_PARTIE = ?");
			$statement->bindParam(1, $idPartie);
			$statement->execute();
			$noJoueurs = $statement->fetchall();
			return $noJoueurs;
		}

		
	}