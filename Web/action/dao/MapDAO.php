<?php
	require_once("Connection.php");

	class MapDAO {
		
		public static function obtenirDonneeMap($idMap, $connection=null) {
			if($connection == null) {
				$connection = Connection::getConnection();
			}

			// Nous prenons seulement les données dont nous avons besoin pour le replay
			$statement = $connection->prepare("SELECT hauteur, largeur FROM map WHERE id = ?");
			$statement->bindParam(1, $idMap);
			$statement->execute();

			$ligne = $statement->fetch();

			$result = [];
			$result["hauteur"] = $ligne[0];
			$result["largeur"] = $ligne[1];
			$result["liste_tuile"] = MapDAO::obtenirListeTuile($idMap, $connection);

			return $result;
		}

		private static function obtenirListeTuile($idMap, $connection) {
			$statement = $connection->prepare("SELECT x, y, id_tuile, arbre FROM tuile_map WHERE id_map = ?");
			$statement->bindParam(1, $idMap);
			$statement->execute();

			$listeTuile = [];

			while($ligne = $statement->fetch()) {
				$tuile = [];
				$tuile["x"] = $ligne[0];
				$tuile["y"] = $ligne[1];
				$tuile["type"] = $ligne[2];
				$tuile["arbre"] = $ligne[3];

				array_push($listeTuile, $tuile);
			}

			return $listeTuile;
		}
	}