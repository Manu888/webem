<?php
	require_once("action/dao/Connection.php");

	class AttributsJoueurDAO {

		// fonction qui retourne la Vie et la Vitesse de base des char selon la balance retrouvée dans la BD
		public static function getStatsBase(){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT * FROM TANK WHERE NOM = 'pointVieChar'");
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				$vie = $statement->fetch();

				$statement = $connection->prepare("SELECT * FROM TANK WHERE NOM = 'vitesseChar'");
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				$vitesse = $statement->fetch();

				$retour = [];
				$retour[] = $vie;
				$retour[] = $vitesse;
				return $retour;
            }
			catch(PDOException $e){
				echo($e->getCode());
				echo($e->getMessage());
				return "ERREUR BD";
			}
		}

		// Mise à jour du joueur en fonction des valeurs envoyés par le site web
		public static function updateJoueur($dtoJoueur){
			try{
				$nomCalcule = AttributsJoueurDAO::calculerNomJoueur($dtoJoueur);

				$connection = Connection::getConnection();
				$statement = $connection->prepare("UPDATE JOUEUR SET ATTR_VIE = ?, ATTR_FORCE = ?, ATTR_AGILETE = ?, ATTR_DEXTERITE = ?, NOM_CALCULE = ?, POINTS_ATTR = ? WHERE username = ?");
				$statement->bindParam(1, $dtoJoueur[0]);
				$statement->bindParam(2, $dtoJoueur[1]);
				$statement->bindParam(3, $dtoJoueur[2]);
				$statement->bindParam(4, $dtoJoueur[3]);
				$statement->bindParam(5, $nomCalcule);
				$statement->bindParam(6, $dtoJoueur[4]);
				$statement->bindParam(7, $dtoJoueur[5]);
				$statement->execute();
				return "OK";			
            }
			catch(PDOException $e){
				echo($e->getCode());
				echo($e->getMessage());
				return "ERREUR BD";
			}
		}

		// Nom calculé redéfini avec les nouvelles valeurs du joueur
		private static function calculerNomJoueur($dtoJoueur){
			// variables pour déterminer les attributs maximum
			$nom_attributs = ["VIE", "FORCE", "AGILETE", "DEXTERITE"];
			$maxA = null;
			$maxB = null;
			$attributA = null;
			$attributB = null;
			
			// suffixes calculés
			$suffixeA = null;
			$suffixeB = null;
			$nomCalcule = null;

			// Exception : quand tous les attributs sont au maximum (30)
			if ($dtoJoueur[0] == 30 && $dtoJoueur[1] == 30 && $dtoJoueur[2] == 30 && $dtoJoueur[3] == 30){
				return $dtoJoueur[5] . " le dominateur";
			}

			// définition du minimum et du maximum
			if ($dtoJoueur[0] >= $dtoJoueur[1]){
				$maxA = $dtoJoueur[0];
				$attributA = $nom_attributs[0];
				$maxB = $dtoJoueur[1];
				$attributB = $nom_attributs[1];
			}
			else {
				$maxA = $dtoJoueur[1];
				$attributA = $nom_attributs[1];
				$maxB = $dtoJoueur[0];
				$attributB = $nom_attributs[0];
			}
			for ($i = 2; $i < count($dtoJoueur) - 1; ++$i){
				if ($dtoJoueur[$i] > $maxA){
					$maxB = $maxA;
					$attributB = $attributA;
					$maxA = $dtoJoueur[$i];
					$attributA = $nom_attributs[$i];
				}
				else if ($dtoJoueur[$i] > $maxB){
					$maxB = $dtoJoueur[$i];
					$attributB = $nom_attributs[$i];
				}
			}
			// Calcul du nom à partir des maximums
			// attribut le plus fort
			switch($attributA){
				case "VIE": 
					if ($maxA >= 10){
						$suffixeA = " l'immortel";
					} 
					else if ($maxA >= 5){
						$suffixeA = " le pétulant";
					}
					else if ($maxA >= 1){
						$suffixeA = " le fougeux";
					}
					else {
						$suffixeA = "";
					} break;
				case "FORCE":
					if ($maxA >= 10){
						$suffixeA = " le tout puissant";
					} 
					else if ($maxA >= 5){
						$suffixeA = " le hulk";
					}
					else if ($maxA >= 1){
						$suffixeA = " le crossfiter";
					}
					else {
						$suffixeA = "";
					} break;
				case "AGILETE": 
					if ($maxA >= 10){
						$suffixeA = " le foudroyant";
					} 
					else if ($maxA >= 5){
						$suffixeA = " le lynx";
					}
					else if ($maxA >= 1){
						$suffixeA = " le prompt";
					} 
					else {
						$suffixeA = "";
					} break;
				case "DEXTERITE": 
					if ($maxA >= 10){
						$suffixeA = " le chirurgien";
					} 
					else if ($maxA >= 5){
						$suffixeA = " l'habile";
					}
					else if ($maxA >= 1){
						$suffixeA = " le précis";
					} 
					else {
						$suffixeA = "";
					} break;
				default:
					$suffixeA = "";
			}
			// 2e attribut le plus fort
			switch($attributB){
				case "VIE": 
					if ($maxB >= 10){
						$suffixeB = " immortel";
					} 
					else if ($maxB >= 5){
						$suffixeB = " pétulant";
					}
					else if ($maxB >= 1){
						$suffixeB = " fougeux";
					} 
					else {
						$suffixeB = "";
					} break;
				case "FORCE":
					if ($maxB >= 10){
						$suffixeB = " tout puissant";
					} 
					else if ($maxB >= 5){
						$suffixeB = " brutal";
					}
					else if ($maxB >= 1){
						$suffixeB = " qui fait du crossfit";
					} 
					else {
						$suffixeB = "";
					} break;
				case "AGILETE": 
					if ($maxB >= 10){
						$suffixeB = " foudroyant";
					} 
					else if ($maxB >= 5){
						$suffixeB = " lynx";
					}
					else if ($maxB >= 1){
						$suffixeB = " prompt";
					} 
					else {
						$suffixeB = "";
					} break;
				case "DEXTERITE": 
					if ($maxB >= 10){
						$suffixeB = " chirurgien";
					} 
					else if ($maxB >= 5){
						$suffixeB = " habile";
					}
					else if ($maxB >= 1){
						$suffixeB = " précis";
					} 
					else {
						$suffixeB = "";
					} break;
				default:
					$suffixeB = "";
			}
			// retourne le nouveau nom calculé composé du username et des suffixes retournées pour chaque attribut max
			$nomCalcule = $dtoJoueur[5] . $suffixeA . $suffixeB;
			return $nomCalcule;
		}
	}