<?php
	require_once("Connection.php");
	require_once("MapDAO.php");

	class ReplayDAO {
		static $correspondanceTypeItem = array(
			"1"=>"canon",
			"2"=>"mitraillette",
			"3"=>"grenade",
			"4"=>"shotgun",
			"5"=>"piege",
			"6"=>"guide",
			"7"=>"spring");

		public static function obtenirDonneePartie($noPartie) {
			$connection = Connection::getConnection();
			
			$result = [];

			// noPartie représente le numéro de la partie parmi les cinq dernières jouées
			$idPartie = ReplayDAO::determinerIdPartie($connection, $noPartie);
			// Obtention des données de la map
			$idMap = ReplayDAO::obtenirIdMap($connection, $idPartie);
			$result["map"] = MapDAO::obtenirDonneeMap($idMap, $connection);

			// Obtention des enregistrements de la partie
			$listeEnregistrementPonctuel = ReplayDAO::obtenirListeIdEnregistrementPonctuel($connection, $idPartie);
			$result["liste_enregistrement"] = $listeEnregistrementPonctuel;

			// on donne le premier enregistrement pour trouver à partir de cellui-ci les infos des joueurs
			$result["liste_info_joueur"] = ReplayDAO::obtenirListeInfoJoueur($connection, $listeEnregistrementPonctuel[0]);
			// On détermine le id du gagnant
			$result["id_gagnant"] = ReplayDAO::determinerIdGagnant($connection, $idPartie);

			return $result;
		}

		private static function determinerIdPartie($connection, $noPartie) {
			$statement = $connection->prepare("SELECT id FROM partie ORDER BY id");
			$statement->bindParam(1, $idPartie);
			$statement->execute();

			$ligne = null;
			for($i = 1; $i <= $noPartie; $i++) {
				$ligne = $statement->fetch();	
			}

			return $ligne[0];
		}

		private static function obtenirIdMap($connection, $idPartie) {
			$statement = $connection->prepare("SELECT id_map FROM partie WHERE id = ?");
			$statement->bindParam(1, $idPartie);
			$statement->execute();

			$ligne = $statement->fetch();

			return $ligne[0];
		}

		private static function determinerIdGagnant($connection, $idPartie) {
			$statement = $connection->prepare("SELECT ID_JOUEUR_GAGNANT FROM partie WHERE id = ?");
			$statement->bindParam(1, $idPartie);
			$statement->execute();

			return $statement->fetch()[0];
		}

		private static function obtenirListeIdEnregistrementPonctuel($connection, $idPartie) {
			// Il est important que les enregistrements soient pris en ordre chronologique
			$statement = $connection->prepare("SELECT id, temps, resolution, hauteur_mur FROM enregistrement WHERE id_partie = ? ORDER BY temps ASC");
			$statement->bindParam(1, $idPartie);
			$statement->execute();

			$listeEnregistrementPonctuel = [];
			while($ligne = $statement->fetch()) {
				$enregistrementPonctuel = [];
				$enregistrementPonctuel["id"] = (int)$ligne[0];
				$enregistrementPonctuel["temps"] = (float)str_replace(",", ".", $ligne[1]);
				$enregistrementPonctuel["resolution"] = (float)str_replace(",", ".", $ligne[2])*1000; // on convertit en float et en milisecondes
				$enregistrementPonctuel["hauteur_mur"] = (float)str_replace(",", ".", $ligne[3]);
				$enregistrementPonctuel["liste_enr_tank"] = ReplayDAO::obtenirListeEnregistrementTank($connection, $ligne[0]);
				$enregistrementPonctuel["liste_enr_item"] = ReplayDAO::obtenirListeEnregistrementItem($connection, $ligne[0]);
				$enregistrementPonctuel["liste_enr_balle"] = ReplayDAO::obtenirListeEnregistrementBalle($connection, $ligne[0]);
				

				array_push($listeEnregistrementPonctuel, $enregistrementPonctuel);
			}

			return $listeEnregistrementPonctuel;
		}

		private static function obtenirListeInfoJoueur($connection, $premierEnr) {
			$idEnr =  $premierEnr["id"];
			$statement = $connection->prepare("SELECT no_joueur FROM enr_position_joueur WHERE ID_ENR = ?");
			$statement->bindParam(1, $idEnr);
			$statement->execute();
			$listeIdJoueur = $statement->fetchall();

			$statement = $connection->prepare("SELECT id, username FROM joueur WHERE id IN (?, ?)");
			$statement->bindParam(1, $listeIdJoueur[0][0]);
			$statement->bindParam(2, $listeIdJoueur[1][0]);
			$statement->execute();

			$listeEnrTank = $premierEnr["liste_enr_tank"];
			$listeInfoJoueur = [];

			$no = 0; // On assigne un numéro arbitraire au joueur pour la partie
			while($paire_id_nom = $statement->fetch()) {
				$id = $paire_id_nom[0];
				$nom = $paire_id_nom[1];
				$no++;
				foreach($listeEnrTank as $enrTank) {
					if($enrTank["id"] == $id) {
						$couleur = ReplayDAO::obtenirCouleurTank($connection, $id);
						$vie = $enrTank["vie"];
						$listeInfoJoueur[$id] = array("nom"=>$nom, "couleur"=>$couleur, "vie_max"=>$vie, "no"=>$no);
					}
				}

			}

			return $listeInfoJoueur;
		}

		private static function obtenirListeEnregistrementTank($connection, $idEnr) {
			$statement = $connection->prepare("SELECT no_joueur, vie, x, y, z, h, p, r FROM enr_position_joueur WHERE ID_ENR = ?");
			$statement->bindParam(1, $idEnr);
			$statement->execute();

			$listeEnregistrementTank = [];

			while($ligne = $statement->fetch()) {
				$enregistrementTank = [];
				$enregistrementTank["id"] = (int)$ligne[0];
				$enregistrementTank["couleur"] = ReplayDAO::obtenirCouleurTank($connection, (int)$ligne[0]);
				$enregistrementTank["vie"] = (float)str_replace(",", ".", $ligne[1]);
				$enregistrementTank["x"] =(float)str_replace(",", ".", $ligne[2]);
				$enregistrementTank["y"] = (float)str_replace(",", ".", $ligne[3]);
				$enregistrementTank["z"] = (float)str_replace(",", ".", $ligne[4]);
				$enregistrementTank["h"] = (float)str_replace(",", ".", $ligne[5]);
				$enregistrementTank["p"] = (float)str_replace(",", ".", $ligne[6]);
				$enregistrementTank["r"] = (float)str_replace(",", ".", $ligne[7]);
				

				array_push($listeEnregistrementTank, $enregistrementTank);
			}

			return $listeEnregistrementTank;
		}

		private static function obtenirCouleurTank($connection, $noJoueur) {
			$statement = $connection->prepare("SELECT tank_color_rouge, tank_color_vert, tank_color_bleu FROM joueur WHERE id = ?");
			$statement->bindParam(1, $noJoueur);
			$statement->execute();

			$ligne = $statement->fetch();
			$r =  (float)str_replace(",", ".", $ligne[0]);
			$g = (float)str_replace(",", ".", $ligne[1]);
			$b = (float)str_replace(",", ".", $ligne[2]);
			$couleur = [$r, $g, $b];

			return $couleur;
		}

		private static function obtenirListeEnregistrementBalle($connection, $idEnr) {
			$statement = $connection->prepare("SELECT x, y, z FROM enr_balle WHERE ID_ENR = ?");
			$statement->bindParam(1, $idEnr);
			$statement->execute();

			$listeEnregistrementBalle = [];

			while($ligne = $statement->fetch()) {
				$enregistrementBalle = [];
				$enregistrementBalle["x"] = (float)str_replace(",", ".", $ligne[0]);;
				$enregistrementBalle["y"] = (float)str_replace(",", ".", $ligne[1]);;
				$enregistrementBalle["z"] = (float)str_replace(",", ".", $ligne[2]);;

				array_push($listeEnregistrementBalle, $enregistrementBalle);
			}

			return $listeEnregistrementBalle;
		}

		private static function obtenirListeEnregistrementItem($connection, $idEnr) {
			$statement = $connection->prepare("SELECT x, y, z, id_item FROM enr_item WHERE ID_ENR = ?");
			$statement->bindParam(1, $idEnr);
			$statement->execute();

			$listeEnregistrementItem = [];

			while($ligne = $statement->fetch()) {
				$enregistrementItem = [];
				$enregistrementItem["x"] = (float)str_replace(",", ".", $ligne[0]);;
				$enregistrementItem["y"] = (float)str_replace(",", ".", $ligne[1]);;
				$enregistrementItem["z"] = (float)str_replace(",", ".", $ligne[2]);;
				$enregistrementItem["type"] = ReplayDAO::$correspondanceTypeItem[$ligne[3]];

				array_push($listeEnregistrementItem, $enregistrementItem);
			}

			return $listeEnregistrementItem;
		}
	}