<?php

	class Connection {
		
		private static $connection = null;
		// Singletion de connection qui permet d'obtenir l'objet duquel on va pouvoir travailler
		public static function getConnection() {
			if (Connection::$connection == null) {
				Connection::$connection = new PDO("oci:dbname=DECINFO", "e0928799", "A");
				Connection::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				Connection::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			}

			return Connection::$connection;
		}
	}