 <?php
	require_once("Connection.php");

	class InfoPlayerDAO {
		public $result;

		public function getInfoPlayer($nomjoueur) {
			try{
				// On récupère les informations d'un joueur à partir de son username
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT * FROM JOUEUR WHERE USERNAME = ? ORDER BY USERNAME");
				$statement->bindParam(1, $nomjoueur);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				
		
				return $this->result = $statement->fetchAll();
			}
			catch(PDOException $e){
				echo($e->getCode());
			}

		}

		public function getInfoPlayerID($idJoueur) {
			try{
				// On récupère les informations d'un joueur à partir de son ID
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT * FROM JOUEUR WHERE ID = ? ");
				$statement->bindParam(1, $idJoueur);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				
		
				return $this->result = $statement->fetchAll();
			}
			catch(PDOException $e){
				echo($e->getCode());
			}

		}

		public function getPrefMap($idjoueur){
			try{	
				// On récupère les ID MAP des parties que le joueur dont on a le ID à le plus joué, gagné ou perdu
				$connection = Connection::getConnection();
				$statement = $connection ->prepare("SELECT ID_MAP FROM PARTIE WHERE ID_JOUEUR_GAGNANT = ? OR ID_JOUEUR_PERDANT = ? GROUP BY ID_MAP ORDER BY COUNT(ID_MAP) DESC");
				$statement->bindParam(1, $idjoueur);
				$statement->bindParam(2, $idjoueur);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				$listGame = $statement->fetchAll();
				
				// On récupère les informations d'une carte à partir des ID qu'on a récupéré plus haut
				$statement = $connection ->prepare("SELECT * FROM MAP WHERE ID = ?");
				$statement->bindParam(1,$listGame[0]["ID_MAP"]);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				$map = $statement->fetchAll();
		
				return $map;
			}
			catch(PDOException $e){
				echo($e->getCode());
				echo($e->getMessage());
			}
		}

		public function getArmesPref($idjoueur){
			try{
				// On récupère les ID d'arme qu'un joueur à le plus tiré et on les ordonne selon la quantité de tir
				$connection = Connection::getConnection();
				$statement = $connection ->prepare("SELECT ID_ARME FROM SHOTS_FIRED WHERE ID_JOUEUR = ? GROUP BY ID_ARME ORDER BY COUNT(NB_TIRS) DESC");
				$statement->bindParam(1,$idjoueur);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				$armes = $statement->fetchAll();
				
				// On récupère les informations des armes dont on a ordonner les ID plus haut
				$statement = $connection ->prepare("SELECT * FROM ARMES WHERE ID = ? OR ID = ?");
				$statement->bindParam(1,$armes[0]["ID_ARME"]);
				$statement->bindParam(2,$armes[1]["ID_ARME"]);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();

				return $infoArme = $statement->fetchAll();
			}
			catch(PDOException $e){
				echo($e->getCode());
				echo($e->getMessage());
			}
		}

		public function getBestPlayers(){
			try{
				// On récupère les information pour le Hall of Fame à partir de la BD selon un ordre de niveau et de ratio de victoire
				// DECODE sert à modifier les valeur zéro possible de la BD et les remplacer par des 1
				// NVL sert à empêcher la récupération de NULL valeur de la BD puisqu'on fait des opérations mathémathiques par la suite 			
				$connection = Connection::getConnection();
				$statement = $connection ->prepare("SELECT ID, USERNAME, NVL(NIVEAU,0) as NIV, decode(NB_PARTIES_JOUEES,0,1,(NVL(NB_PARTIES_GAGNEES,0)/NVL(NB_PARTIES_JOUEES,1))) as WINRATE FROM JOUEUR ORDER BY NIV DESC, WINRATE DESC");
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				
				return  $statement->fetchAll();
			}
			catch(PDOException $e){
				
				echo($e->getCode());
				echo($e->getMessage());
			}

		}

		public function authenticate($username, $password) {
			$visibility = 0;

			
			$connection = Connection::getConnection();
			$statement = $connection->prepare("SELECT pswd FROM joueur WHERE username = ?");
			$statement->bindParam(1, $username);
			try{
				$statement->execute();

				$result = $statement->fetch(PDO::FETCH_ASSOC);
				$pswd = $result["PSWD"];
				if ($pswd != "" && $pswd != null){
					if(password_verify($password,$pswd))
					{
						$visibility = 1;
					}
				}
			}
			catch(PDOException $e){
				echo($e->getCode());
				echo($e->getMessage());
			}
			return $visibility;
		}

		public function nouveauJoueur($joueur){

			$rouge = str_replace(".",",",strval($joueur["couleur"]["rouge"]/255.0));
			$vert =  str_replace(".",",",strval($joueur["couleur"]["vert"]/255.0));
			$bleu = str_replace(".",",",strval($joueur["couleur"]["bleu"]/255.0));

			//protection vs cross site scripting
			$prenom = "";
			$nom = "";
			$username = "";
			$courriel = "";
			$listeCarac = [">","<","{","}"];
			foreach($listeCarac as $char){
				$prenom = str_replace($char,"",$joueur["prenom"]);
				$nom = str_replace($char,"",$joueur["nom"]);
				$username = str_replace($char,"",$joueur["username"]);
				$courriel = str_replace($char,"",$joueur["courriel"]);
			}
			
			$connection = Connection::getConnection();

			$statement = $connection->prepare("INSERT INTO joueur(prenom,nom,username,pswd,courriel,tank_color_rouge,tank_color_vert,tank_color_bleu,
																	xp,niveau,nb_parties_jouees,nb_parties_gagnees,points_attr,attr_vie,attr_force,attr_agilete,
																	attr_dexterite,nom_calcule) 
													VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			$statement->bindParam(1, $prenom);
			$statement->bindParam(2, $nom);
			$statement->bindParam(3, $username);
			$statement->bindParam(4, $joueur["password"]);
			$statement->bindParam(5, $courriel);
			$statement->bindValue(6, $rouge);
			$statement->bindValue(7, $vert);
			$statement->bindValue(8, $bleu);
			$statement->bindValue(9, 0);
			$statement->bindValue(10, 0);
			$statement->bindValue(11, 0);
			$statement->bindValue(12, 0);
			$statement->bindValue(13, 0);
			$statement->bindValue(14, 0);
			$statement->bindValue(15, 0);
			$statement->bindValue(16, 0);
			$statement->bindValue(17, 0);
			$statement->bindParam(18, $joueur["username"]);
			try{
				$statement->execute();
			}
			catch(PDOException $e){
				echo($e->getCode());
				echo($e->getMessage());
			}
		}

		public function modifJoueur($joueur){

			$rouge = str_replace("0.","0,",strval($joueur["couleur"]["rouge"]/255.0));
			$vert =  str_replace("0.","0,",strval($joueur["couleur"]["vert"]/255.0));
			$bleu = str_replace("0.","0,",strval($joueur["couleur"]["bleu"]/255.0));

			//protection vs cross site scripting
			$prenom = "";
			$nom = "";
			$username = "";
			$courriel = "";
			$listeCarac = [">","<","{","}"];
			foreach($listeCarac as $char){
				$prenom = str_replace($char,"",$joueur["prenom"]);
				$nom = str_replace($char,"",$joueur["nom"]);
				$username = str_replace($char,"",$joueur["username"]);
				$courriel = str_replace($char,"",$joueur["courriel"]);
			}


			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("UPDATE joueur SET prenom = ?, nom = ?, username = ?, pswd = ?, courriel = ?,
																tank_color_rouge = ?, tank_color_vert = ?,tank_color_bleu = ?
															WHERE id = ?");

			$statement->bindParam(1, $prenom);
			$statement->bindParam(2, $nom);
			$statement->bindParam(3, $username);
			$statement->bindParam(4, $joueur["password"]);
			$statement->bindParam(5, $courriel);
			$statement->bindParam(6, $rouge);
			$statement->bindParam(7, $vert);
			$statement->bindParam(8, $bleu);
			$statement->bindValue(9, $joueur["id"]);
			try{
				$statement->execute();
			}
			catch(PDOException $e){
				echo($e->getCode());
				echo($e->getMessage());
			}

		}

		public function usernameAvailable($username){
			$connection = Connection::getConnection();
			$statement = $connection->prepare("SELECT username FROM joueur WHERE username = ?");
			$statement->bindParam(1, $username);
			$statement->execute();

			$result = $statement->fetch(PDO::FETCH_ASSOC);
			$user = $result["USERNAME"];
			if ($user != "" && $user != null){
				return false;
			}
			else {
				return true;
			}
		}

		public function emailAvailable($email){
			$connection = Connection::getConnection();
			$statement = $connection->prepare("SELECT courriel FROM joueur WHERE courriel = ?");
			$statement->bindParam(1, $email);
			$statement->execute();

			$result = $statement->fetch(PDO::FETCH_ASSOC);
			$user = $result["COURRIEL"];
			if ($user != "" && $user != null){
				return false;
			}
			else {
				return true;
			}
		}

		public function changePasswordFromEmail($email){
			$connection = Connection::getConnection();
			$statement = $connection->prepare("SELECT courriel FROM joueur WHERE courriel = ?");
			$statement->bindParam(1, $email);
			$statement->execute();

			$result = $statement->fetch(PDO::FETCH_ASSOC);
			$user = $result["COURRIEL"];
			if ($user != "" && $user != null){
				$pswd = password_hash("SinisterS1x!",PASSWORD_BCRYPT);
				$statement = $connection->prepare("UPDATE joueur SET pswd = ? WHERE courriel = ?");
				$statement->bindParam(1, $pswd);
				$statement->bindParam(2, $email);
				$statement->execute();
				
				return true;
			}
			else {
				return false;
			}
		}

		public function recherche($username){
			try{
				// On récupère tout les joueurs dont le nom débute par le préfix inséré par le joueur
			$connection = Connection::getConnection();
			$statement = $connection->prepare("SELECT * FROM joueur WHERE username LIKE ?");
			$name = $username .'%';
			$statement->bindParam(1, $name);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			

			return $statement->fetchAll();
			}
			catch(PDOException $e){
				echo($e->getCode());
			}
		}
	}