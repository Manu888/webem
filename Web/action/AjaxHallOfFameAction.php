<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/InfoPlayerDAO.php");
	require_once("action/dao/HallOfFameDAO.php");

	class AjaxHallOfFameAction extends CommonAction {
		public $result = null;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			//$dao = new InfoPlayerDao();
			//$this->result = $dao->getBestPlayers();
			$this->result = HallOfFameDAO::listeHallOfFame();
		}
	}