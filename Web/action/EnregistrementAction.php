<?php
	require_once("CommonAction.php");
	require_once("dao/InfoPlayerDAO.php");

	class EnregistrementAction extends CommonAction {
		public $manqueInfo = false;
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if (isset($_POST["username"])
				&& isset($_POST["prenom"])
				&& isset($_POST["nom"])
				&& isset($_POST["confirmpassword"])
				&& isset($_POST["courriel"])) {
				#changer la couleurvaleur hexa de la couleur en liste de 3 couleurs de 0-255
				$couleur = $this->hex2rgb($_POST["couleur"]);

				#initialiser un joueur a passer au DAO
				$joueur = array("prenom"=>$_POST["prenom"],
								"nom" =>$_POST["nom"],
								"username" => $_POST["username"],
								"password" =>password_hash($_POST["password"],PASSWORD_BCRYPT),
								"courriel" =>$_POST["courriel"],
								"couleur" =>$couleur);				
				
				#insérer le nouveau joueur dans la BD
				$dao = new InfoPlayerDao();
				$dao->nouveauJoueur($joueur);

				//si l'insertion est réussi le user est connecté
				$visibility = $dao->authenticate($_POST["username"], $_POST["confirmpassword"]);
				if ($visibility > 0) {
					$_SESSION["username"] = $_POST["username"];
					$_SESSION["visibility"] = $visibility;
					header("location:fiche.php?username=".$_SESSION["username"]);
					unset($_POST["username"]);
					exit;
				}
			}
			else if(isset($_POST["prenom"]) 
					|| isset($_POST["nom"])
					|| isset($_POST["username"])
					|| isset($_POST["password"])
					|| isset($_POST["courriel"])){
				
					$this->manqueInfo = true;
			}
		}

		#fonction pour générer une liste rgb à partir d'une couleur en hexa
		protected function hex2rgb($color) {
			   if ( $color[0] == '#' ) {			
				   $color = substr( $color, 1 );			
			   }
			   $hex = array("rouge"=> $color[0] . $color[1],
							"vert"=> $color[2] . $color[3],
							"bleu"=> $color[4] . $color[5] );

			   $rgb = array_map('hexdec', $hex);
			
			   return $rgb;
		   }
	}
