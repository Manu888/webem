<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/HallOfFameDAO.php");

	class HallOfFameAction extends CommonAction {

		public $resultat = null;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$this->resultat = HallOFFameDAO::listeHallOfFame();
			
		}
	}