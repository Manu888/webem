<?php
    require_once("action/dao/InfoPlayerDAO.php");
    require_once("action/CommonAction.php");

	class FicheAction extends CommonAction {
        public $result;
        public $mapPref;
        public $armesPref;
        public $hallOfFame;
        
        public function __construct() {
            parent::__construct(parent::$VISIBILITY_PUBLIC); // VALEUR DE LA VISIBILITY A CHANGER POUR LE METTRE A MEMBRE
		}

		protected function executeAction() {
            // VALEUR BIDON DE USERNAME
            //$_SESSION["username"] = "jordy";
            $dao = new InfoPlayerDAO();
            // Si on est connecté, on a stocker le nom de user dans la variable de session et on l'utilise pour retrouver ses infos
            if(isset($_GET["username"])){
                $this->result = $dao->getInfoPlayer($_GET["username"]);
                
                if(sizeof($this->result)>0){
                    $this->mapPref = $dao->getPrefMap($this->result[0]["ID"]);
                    $this->armesPref = $dao->getArmesPref($this->result[0]["ID"]);
                    $this->hallOfFame = $dao->getBestPlayers();
                }
                else{
                    // Si on a pas d'usager qui correspond a un username valide , on redirige l'usager sur la page de recherche
                    header("location:recherche.php");
				    exit;
                }
            }
            else{ // Si on a pas donner le nom de user à affiche, on retourne à la page de recherche
				header("location:recherche.php");
				exit;
            }
			 
		}
	}