<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/LastPlayedDAO.php");

	class AjaxLastPlayedAction extends CommonAction {

		public $result = null;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			//aller chercher idJoueur dans le data du ajax
			$this->result = LastPlayedDAO::listeLastPlayed();
		}
	}