<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/InfoPlayerDAO.php");

	class IndexAction extends CommonAction {
		public $wrongLogin = false;
		public $userBlocked = false;
		public $tentatives = 0;
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			if (isset($_POST["username"])) {
				$username = $_POST["username"];
				//si le user est banned il ne peut pas se connecter
				if(isset($_SESSION["bannedUsers"])){
					foreach($_SESSION["bannedUsers"] as $index => $user)
						if($user == $username){
							$this->userBlocked = true;
							return;
						}
				}
				
				$dao = new InfoPlayerDAO();
				$visibility = $dao->authenticate($username, $_POST["pwd"]);
				

				if ($visibility > 0) {
					$_SESSION["visibility"] = $visibility;
					$_SESSION["username"] = $username;
						
					unset($_POST["username"]);
					header("location:fiche.php?username=".$_SESSION["username"]);
					exit;
				}
				else {
					//si la connection ne reussis pas on ajoute le username dans une liste
					if(!isset($_SESSION["wrongLogin"])){
						$_SESSION["wrongLogin"]= array($username=>1);
						$this->tentatives = 1;
					}
					else{
						if(isset($_SESSION["wrongLogin"][$username])){
							$_SESSION["wrongLogin"][$username] += 1;
							$this->tentatives = $_SESSION["wrongLogin"][$username];
							if($_SESSION["wrongLogin"][$username] > 3){
								if(!isset($_SESSION["bannedUsers"])){
									$_SESSION["bannedUsers"][0] = $username;
								}
								else{
									$index = count($_SESSION["bannedUsers"]);
									$_SESSION["bannedUsers"][$index] = $username;
								}
							}
						}
						else{
							$_SESSION["wrongLogin"][$username] = 1;
						}
					}
					$this->wrongLogin = true;
				}
			}
			else if(isset($_SESSION["username"]) && isset($_SESSION["visibility"])){
				header("location:fiche.php?username=".$_SESSION["username"]);
				exit;
			}
		}
	}
