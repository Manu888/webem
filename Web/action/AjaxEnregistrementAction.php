<?php
	require_once("CommonAction.php");
	require_once("dao/InfoPlayerDAO.php");

	class AjaxEnregistrementAction extends CommonAction {

		public $result = null;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$this->result = null;
			$dao = new InfoPlayerDao();
			$resultat = null;
			if($_POST["type"] == "username"){
				$resultat = $dao->usernameAvailable($_POST["valeur"]);
			}
			else if($_POST["type"] == "email"){
				$resultat = $dao->emailAvailable($_POST["valeur"]);
			}

			$this->result = json_encode($resultat);
		}
	}