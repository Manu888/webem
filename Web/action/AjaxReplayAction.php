<?php
	require_once("CommonAction.php");
	require_once("dao/ReplayDAO.php");
	

	class AjaxReplayAction extends CommonAction {

		public $result = [];
		
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
			
		}

		protected function executeAction() {
			$noPartie = $_POST["noPartie"];
			$this->result = ReplayDAO::obtenirDonneePartie($noPartie);

		}
	}