<?php
	require_once("CommonAction.php");
	require_once("dao/InfoPlayerDAO.php");

	class AjaxRechercheAction extends CommonAction {

		public $result = null;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$dao = new InfoPlayerDao();
            $this->result = $dao->recherche($_GET["name"]);

		}
	}