<?php
	require_once("CommonAction.php");
	require_once("dao/InfoPlayerDAO.php");

	class ModifUserAction extends CommonAction {
		public $joueur;
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			if (isset($_SESSION["username"])) {
				$dao = new InfoPlayerDao();
				$this->joueur = $dao->getInfoPlayer($_SESSION["username"]);
				$this->joueur[0]["COULEUR"] = $this->rgb2hex(floatval(str_replace(",","0.",$this->joueur[0]["TANK_COLOR_ROUGE"]))*255,
															floatval(str_replace(",","0.",$this->joueur[0]["TANK_COLOR_VERT"]))*255,
															floatval(str_replace(",","0.",$this->joueur[0]["TANK_COLOR_BLEU"]))*255);				
			}

			$joueur = array();
			

			if (isset($_POST["username"]) && isset($_POST["truePassword"])) {
				$dao = new InfoPlayerDao();
				if($dao->authenticate($_SESSION["username"],$_POST["truePassword"])){
					$couleur = $this->hex2rgb($_POST["couleur"]);
					if(isset($_POST["confirmpassword"]) && $_POST["confirmpassword"] != ""){
						$joueur["password"] = password_hash($_POST["confirmpassword"],PASSWORD_BCRYPT);
					}
					else{
						$joueur["password"] = password_hash($_POST["truePassword"],PASSWORD_BCRYPT);
					}
					$joueur["id"] = $this->joueur[0]["ID"];
					$joueur["prenom"] = $_POST["prenom"];
					$joueur["nom"] = $_POST["nom"];
					$joueur["username"] = $_POST["username"];
					$joueur["courriel"] = $_POST["courriel"];
					$joueur["couleur"] = $couleur;
				
					$dao->modifJoueur($joueur);
					
					unset($_POST["username"]);
					header("location:fiche.php?username=".$_SESSION["username"]);				
					exit;
				}
			}
		}

		protected function hex2rgb($color) {
			if ( $color[0] == '#' ) {			
				$color = substr( $color, 1 );			
			}
			$hex = array("rouge"=> $color[0] . $color[1],
						 "vert"=> $color[2] . $color[3],
						 "bleu"=> $color[4] . $color[5] );

			$rgb = array_map('hexdec', $hex);
		 
			return $rgb;
		}

		protected function rgb2hex($red,$green,$blue) {
			if($red > 15){
				$rouge = dechex($red);
			}
			else{
				$rouge = "0" . dechex($red);
			}
			if($green > 15){
				$vert = dechex($green);
			}
			else{
				$vert = "0" . dechex($green);
			}
			if($blue > 15){
				$bleu = dechex($blue);
			}
			else{
				$bleu = "0" . dechex($blue);
			}
			
			$hex = "#" . $rouge . $vert . $bleu;
		 
			return $hex;
		}
	}
