<?php
	require_once("action/EnregistrementAction.php");

	$action = new EnregistrementAction();
	$action->execute();

	$manqueInfo = $action->manqueInfo;

	require_once("partial/header.php");
?>
<script src="js/enregistrement.js"></script>
<div class="enregistrement-form-frame">
	<h2>Nouvel utilisateur</h2>
		<form id="formLayout" action="enregistrement.php" method="post">
			
			<div class="form-label">
				<label for="prenom">Prenom : </label>
			</div>
			<div class="form-input">
				<input type="text" required name="prenom" id="prenom" maxlength="20"/>
			</div>
			<div class="form-label">
				<label for="nom">Nom : </label>
			</div>
			<div class="form-input">
				<input type="text" required name="nom" id="nom" maxlength="20"/>
			</div>
			<div class="form-label">
				<label for="username">Nom d'usager : </label>
			</div>
			<div class="form-input">
				<input type="text" onfocusout="checkSiDispo('username')" required name="username" id="username" maxlength="20"/>
			</div>
			<p id="validateUsername"></p>			
			<div class="form-separator"></div>
			
			<div class="form-label">
				<label for="password">Mot de passe : </label>
			</div>
			<div class="form-input" >
				<input type="password" onfocusout="checkPassword()" required name="password" id="password" />
				<div onclick="showCriteres()"> &#10068; Critères </div>
				<p id="criteres"><span>Le mot de passe doit avoir au moins 9 caractères,</br>dont au moins une lettre, un chiffre et </br>un caractère parmis ! , @ , # , $ , % , ? , * , + , et -</span></p>
				<p id="passwordCheck"></p>
			</div>
			<div class="form-label">
				<label for="password">Confirmer votre Mot de passe : </label>
			</div>
			<div class="form-input">
				<input type="password" onfocusout="confirmPassword()" name="confirmpassword" id="confirmpassword" />
				<p id="confirmPasswordCheck"></p>
			</div>
			<div class="form-separator"></div>
			<div class="form-label">
				<label for="courriel">Courriel : </label>
			</div>
			<div class="form-input">
				<input type="text" onfocusout="checkSiDispo('email')" name="courriel" id="courriel" maxlength="40"/>
				<p id="validateEmail"></p>
			</div>
			<div class="form-label">
				<label for="couleur">Couleur de votre tank : </label>
			</div>
			<div class="form-input">
				<input type="color" name="couleur" id="couleur" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				&nbsp;
			</div>
			<div class="form-input">
				<button type="submit">Créer mon profil</button>
			</div>
			<div class="form-separator"></div>
		</form>


<?php
	require_once("partial/footer.php");