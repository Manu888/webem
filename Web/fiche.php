<?php
	require_once("action/FicheAction.php");

	$action = new FicheAction();
	$action->execute();
    // On fait la requete qui recupere tous les infos du joueur
    // y comprit ses armes pref, sa map pref et sa position dans la HallofFame si il y a
    $result = $action->result;
    $mapPref = $action->mapPref;
    $armesPref = $action->armesPref;
    $bestPlayers = $action->hallOfFame;
    $inHallOfFame = false;

    // On remplace les virgules par des 0. pour les valeurs de couleurs (nécessaire au WEBGL)
    
    $rouge = floatval(str_replace(",","0.",$result[0]["TANK_COLOR_ROUGE"]));
    $bleu = floatval(str_replace(",","0.",$result[0]["TANK_COLOR_BLEU"]));
    $vert = floatval(str_replace(",","0.",$result[0]["TANK_COLOR_VERT"]));
    $array = array();
    array_push($array,$rouge,$vert,$bleu);
    
    require_once("partial/header.php");
?>
<script> let couleur = <?=json_encode($array)?>; </script>
<script> let infos = <?=json_encode($result)?>; </script>
<script src="js/fiche.js"></script>
<script src="js/util/glMatrix-0.9.5.min.js"></script>
<script src="js/webgl/shader.js"></script>
<script src="js/webgl/objet3d.js"></script>
<script src="js/webgl/ciel3d.js"></script>
<script src="js/webgl/tank3d.js"></script>

<!-- Pour chacun des champs on le remplit avec l'info qu'on a récupérer de la BD sinon on affiche qu'il n'y en a pas  -->
<h2 "font-weight-bold"> Fiche personnel de <?= $result[0]["USERNAME"] ?></h2>
<article id="identification">
    <div class="container">
        <div class="row">
            <div class="col-md">
                <h3 class="text-primary" >Nom du joueur : </h3><h4 id="username"><?= $result[0]["USERNAME"] ?></h4>
            </div>
            <div class="col-md">
                <h3 class="text-primary" >Nom calculé : </h3><h4 id="nameCalculated"><?= $result[0]["NOM_CALCULE"] ?></h4>
            </div>
            <div class="col-md">
                <h3 class="text-primary" >Niveau : </h3><h4 id="level"><?= $result[0]["NIVEAU"] ?></h4>
            </div>
        </div>
        <div class="d-flex flex-row align-items-center justify-content-around">
        <div>
            <h3 class="text-primary">Son tank : </h3><canvas id="tank1" width="250" height="180"> </canvas>
        </div>
            <div id="stats">
                <h3 class="text-primary">Parties jouées : </h3><h4 id="playedGame"><?= $result[0]["NB_PARTIES_JOUEES"] ?></h4>
                <h3 class="text-primary">Parties gagnées : </h3><h4 id="wonGame"><?=$result[0]["NB_PARTIES_GAGNEES"]?></h4>
                <h3 class="text-primary">Ratio de victoire : </h3>
                <?php   // CALCUL DU POURCENTAGE DE VICTOIRE EN PHP
                if ($result[0]["NB_PARTIES_JOUEES"] > 0 ){?>
                    <h4 id="ratioGame"><?=strval(round(($result[0]["NB_PARTIES_GAGNEES"] / $result[0]["NB_PARTIES_JOUEES"])*100)) . '%'?></h4>
                <?php }else { ?>
                    <h4 id="ratioGame">--</h4>
                <?php } ?>

            </div>
        </div>
        <div class="d-flex flex-row justify-content-around align-items-center">
            <h3 class="text-primary">Niveau préféré : </h3><h4 id="niveauPref"><?php if(!empty($mapPref)) echo ($mapPref[0]["NOM"]); else echo ("Aucune partie enregistrée");?></h4>
            <h3 class="text-primary">Arme préférée : </h3><h4 id="arme1Pref"><?php if(!empty($armesPref)) echo ($armesPref[0]["NOM"]); else echo ("Aucune arme enregistrée");?></h4>
            <h3 class="text-primary">Arme préférée : </h3><h4 id="arme2Pref"><?php if(!empty($armesPref)) echo ($armesPref[1]["NOM"]); else echo ("Aucune arme enregistrée");?></h4>
        </div>
        <div class="d-flex flex-row justify-content-around align-items-center">
        <div>
            <h3 class="text-primary"> Hall of Fame : </h3><h4 ><?php
            // On parcours la liste des joueurs du top 10 du halloffame et on affiche son rang et son badge si le joueur actuel en fait partie 
            for($i = 0; $i < sizeof($bestPlayers); $i++){
                if($bestPlayers[$i]["USERNAME"] == $result[0]["USERNAME"]){
                    $inHallOfFame = true;
                    ?> Rang : <?=$i+1?></h4></div><img src="images/rank<?=$i?>.png"><?php
                }
            }
            if(!$inHallOfFame){
                ?> Rang : non classé </h4>
            <?php } ?>
        </div>
    </div>
</article>

<?php
	require_once("partial/footer.php");