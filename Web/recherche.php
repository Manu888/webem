<?php
	require_once("action/RechercheAction.php");

	$action = new RechercheAction();
	$action->execute();

    require_once("partial/header.php");
?>
<script src="js/recherche.js"></script>

<h2 "font-weight-bold">Rechercher un joueur</h2>

<div id="champRecherche" class="mt-4">
	<input type="text" name="name" id="name" onkeypress= 'return press(event)' />
	<!-- Quand on appuie sur le bouton Recherche, on lance l'appel à la BD-->
	<!-- Le bouton ENTER sur le champ fonctionne aussi -->
	<button type="button" onclick="recherche()" id="bouttonRecherche">Rechercher</button>
</div>
<div class="list-group joueur"id="resultatRecherche">

</div>
<div class="d-flex justify-content-center">
	<ul class="pagination mt-4" id="pagination">

	</ul>
</div>
<!-- TEMPLATE D'UN BLOC POUR UN USAGER -->
<script type="player-template" id="player-template" >
	<button class="list-group-item list-group-item-action flex-column align-items-center buttonFiche">
		<div class="d-flex w-100 justify-content-around">
			<h4 class="text-primary">Username :  </h4>
			<h4 class="mt-1 username"></h4>
		</div>
		<div class="d-flex w-100 justify-content-around">
			<h4 class="text-primary">Niveau :  </h4>
			<h4 class=" mt-1 level"></h4>
		</div>
		<div class="d-flex w-100 justify-content-around">
			<h4 class="text-primary">Nombre de parties  :  </h4>
			<h4 class="mt-1 gamePlayed"></h4>
		</div>
		<div class="d-flex w-100 justify-content-around">
			<h4 class="text-primary">Ratio de victoire :  </h4>
			<h4 class="mt-1 winRate"></h4>
		</div>
	</button>

</script>

<?php
	require_once("partial/footer.php");