<?php
	require_once("action/HallOfFameAction.php");

	$action = new HallOfFameAction();
	$action->execute();

	require_once("partial/header.php");
?>
<script src="js/hallOfFame.js"></script>
<script src="js/util/glMatrix-0.9.5.min.js"></script>
<script src="js/webgl/shader.js"></script>
<script src="js/webgl/objet3d.js"></script>
<script src="js/webgl/ciel3d.js"></script>
<script src="js/webgl/tank3d.js"></script>
<script src="js/tankJoueur.js"></script>
<script type="x-template" id="hall-template">
	<td class="positionHall"></td>
	<td class="imageRang"></td>
	<td class="nomJoueur"></td>
	<td class="niveauFavori"></td>
	<td class="tank"></td>
	<td class="ratio"></td>
	<td class="nbParties"></td>
</script>
<div id="page-hall">
	<table id="tableau-hall">
		<tr>
			<th class="th">Position</th>
			<th class="th">Rang</th>
			<th class="th">Nom</th>
			<th class="th">Map Préférée</th>
			<th class="th">Tank</th>
			<th class="th">Ratio Victoires</th>
			<th class="th">Parties Jouées</th>
		</tr>
	</table>
</div>
<?php
	require_once("partial/footer.php");