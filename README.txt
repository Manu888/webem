#Webem_ALL

 Site Web réalisé par Io-Daza Dillon, Philippe Roy, Jordan Villemaire et Manuel Piquette


                                                     _..----.._                                   
                                                    ]_.--._____[                                  
                                                  ___|'--'__..|--._                               
                              __               """    ;            :                              
                            ()_ """"---...__.'""!":  /    ___       :                             
                               """---...__\]..__] | /    [ 0 ]      :                             
                                          """!--./ /      """        :                            
                                   __  ...._____;""'.__________..--..:_                           
                                  /  !"''''''!''''''''''|''''/' ' ' ' \"--..__  __..              
                                 /  /.--.    |          |  .'          \' ' '.""--.{'.            
             _...__            >=7 //.-.:    |          |.'             \ ._.__  ' '""'.          
          .-' /    """"----..../ "">==7-.....:______    |                \| |  "";.;-"> \         
          """";           __.."   .--"/"""""----...."""""----.....H_______\_!....'----""""]       
        _..---|._ __..--""       _!.-=_.            """""""""""""""                   ;"""        
       /   .-";-.'--...___     ." .-""; ';""-""-...^..__...-v.^___,  ,__v.__..--^"--""-v.^v,      
      ;   ;   |'.         """-/ ./;  ;   ;\P.        ;   ;        """"____;  ;.--""""// '""<,     
      ;   ;   | 1            ;  ;  '.: .'  ;<   ___.-'._.'------""""""____'..'.--""";;'  o ';     
      '.   \__:/__           ;  ;--""()_   ;'  /___ .-" ____---""""""" __.._ __._   '>.,  ,/;     
        \   \    /"""<--...__;  '_.-'/; ""; ;.'.'  "-..'    "-.      /"/    `__. '.   "---";      
         '.  'v ; ;     ;;    \  \ .'  \ ; ////    _.-" "-._   ;    : ;   .-'__ '. ;   .^".'      
           '.  '; '.   .'/     '. `-.__.' /;;;   .o__.---.__o. ;    : ;   '"";;""' ;v^" .^        
             '-. '-.___.'<__v.^,v'.  '-.-' ;|:   '    :      ` ;v^v^'.'.    .;'.__/_..-'          
                '-...__.___...---""'-.   '-'.;\     'WW\     .'_____..>."^"-""""""""              
                                      '--..__ '"._..'  '"-;;"""                                   
                                             """---'""""""                                        

 Le projet WebEmAll et TankEmAll se déroule entièrement sur la base de donnée ORACLE de Manuel Piquette. Son nom d'usager est 0928799 et son MDP est 'A'.
 Le site Web est entièrement dépendant du jeu et fonctionne plutôt comme un complément. Ainsi, il ne devrait pas avoir d'interférence entre les deux.
 Nous avons tenté de garder le site Web le plus explicite de sorte que les fonctions parle d'elle-même. Par exemple, lorsqu'on clique sur le bouton
 'Mot de passe oubliée ?', on nous demande notre e-mail afin de nous retourner un mot de passe de secours pour le compte associé. Toutefois, il pourrait
 subsister quelques doutes dans l'esprit de l'usager. Afin de prévenir cela, voici quelques spécifications propres à notre site:
  

-- La fonction de recherche fonctionne selon le principe de préfixe ainsi lorsque l'usager laissera le champ d'entrée vide, le module effectuera une recherche
   tout les usagers. Il est aussi d'appuyer sur la touche ENTER au lieu de cliquer sur le bouton, ce qui facilite la recherche rapide. Une liste d'usager
   s'affichera suite à la recherche avec les informations de base de chacun. Afin d'accèder à la fiche personnelle, vous n'avez qu'à cliquer sur l'usager.

-- La fiche personnelle d'un usager est seulement accèssible via le module de recherche. La fiche personnelle de l'usager est remplie dans un format GET et donc il est aussi 
   possible de saisir l'usager directement dans l'espace dédiée à l'URL. Des précautions ont été prises afin d'empêcher l'usager d'entrer des valeurs érronées dans
   celle-ci. Il est aussi possible que le joueur n'ait pas assez jouer de partie de Tankem pour afficher les statistiques de sa carte préférée ou bien
   de ses armes préférées. Si il fait partie du HallOfFame des joueurs, son rang ainsi que son badge correspondant sera afficher dans sa fiche personelle.


_______( •̪●)
░░░░░░███████ ]▄▄▄▄▄▄▄▄▃
▂▄▅█████████▅▄▃▂
I███████████████████].
◥⊙▲⊙▲⊙▲⊙▲⊙▲⊙▲⊙◤...